<?php

namespace Feldheim\ExportOrders\Cron;
ini_set('memory_limit','-1');
class Exportorders
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected $_OrderCollection;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\State $state,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Sales\Model\ResourceModel\Order\Collection $_OrderCollection,
        \Magento\Checkout\Model\Session $checkoutSession

    ) {
        $this->logger = $loggerInterface;
        $this->_OrderCollection = $_OrderCollection;
        $this->_checkoutSession = $checkoutSession;

    }

    public function execute() {

        /*$this->logger->debug('Feldheim\ExportOrders\Cron\Exportorders');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('mgsg_last_order_exported');
        $sql = "SELECT * FROM " . $tableName;
        $result = $connection->fetchAll($sql);
        $lastOrderExported=$result[0]['lastorder_id'];
// 	 echo $lastOrderExported." this is last order exported <br>";
        $exportedOrder=$this->exportTheseOrders($lastOrderExported, $this->getRealOrderId());
        $connection->update($tableName, ['lastorder_id' => $exportedOrder], ['lastorder_id IN (?)' => $lastOrderExported]);*/

    }

    public function getRealOrderId()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $sql="SELECT MAX(`increment_id`) as last_order FROM `mgsg_sales_order`";
        $connection = $resource->getConnection();
        $result = $connection->fetchAll($sql);
        $lastorderId=$result[0]['last_order'];
        // echo "last order placed is ". $lastorderId;
        return $lastorderId;
    }

    public function exportTheseOrders($exportOrder, $lastOrder){
        $data=array();
        if ($exportOrder==$lastOrder || $exportOrder > $lastOrder){
            return $lastOrder;
        }
        else{
            $heading = array("order_id","date_placed","username","tax","shipping_cost","card_type_string","card_number","card_ccid","card_exp_month","card_exp_year","card_holder","coupondiscount2","Item #","retail price","discount","quantity","unit_price","ship_first_name","ship_last_name","ship_street1","ship_street2","ship_city","ship_state","ship_zip","ship_country","bill_first_name","bill_last_name","bill_street1","bill_street2","bill_city","bill_state","bill_zip","bill_country","special instructions","pointdiscount","giftwrap","taxrate","bill_company","ship_company","email","shipping_method","realtime_confirmation","grand_total","phone_number","weight");
            $today=date("mdY-his");
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
            $outputFile = $directory->getRoot()."/var/feldheim/".$today."_ORDERS.csv";
            echo $outputFile;
            $handle = fopen($outputFile, "w");
            fputcsv($handle,$heading, "\t");
            $i=$exportOrder+1;
            for (;$i<=$lastOrder;$i++){
                //echo "Exporting order ".$i;
                $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
                $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
                $orderData = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($i);
                if (!$orderData) continue;
                $getorderdata = $orderData->getData();
                $billingAddress = $orderData->getBillingAddress();
                $shippingAddress = $orderData->getShippingAddress();
                $payment = $orderData->getPayment();
                if ($payment){
                    $method = $payment->getMethodInstance();
                    $methodTitle = $method->getTitle();
                }
                $orderItems = $orderData->getAllVisibleItems();

                foreach($orderItems as $orderItem){
                    $itemDetail=$orderItem->getData();
                    $data[$i]['order_id']=$i;
                    $formatted_date=date("m/d/Y", strtotime($getorderdata['created_at']));
                    $data[$i]['date_placed']=$formatted_date;
                    $data[$i]['username']=$billingAddress['email'];
                    $data[$i]['tax']=$getorderdata['tax_amount'];
                    $data[$i]['shipping_cost']=$getorderdata['shipping_amount'];
                    $data[$i]['card_type_string']=$methodTitle;
                    $data[$i]['card_number']='';
                    $data[$i]['card_ccid']='';
                    $data[$i]['card_exp_month']='';
                    $data[$i]['card_exp_year']='';
                    $data[$i]['card_holder']='';
                    $data[$i]['coupondiscount2']=$itemDetail['discount_amount'];
                    $data[$i]['sku']=$itemDetail['product_id'];
                    $data[$i]['price']=$itemDetail['price'];
                    $data[$i]['discount']=$itemDetail['discount_amount'];
                    $data[$i]['quantity']=$itemDetail['qty_ordered'];
                    $data[$i]['unit_price']=$itemDetail['price'];
                    $data[$i]['ship_first_name']=$shippingAddress['firstname'];
                    $data[$i]['ship_last_name']=$shippingAddress['lastname'];
                    $data[$i]['ship_street1']=$shippingAddress['street'];
                    $data[$i]['ship_street2']='';
                    $data[$i]['ship_city']=$shippingAddress['city'];
                    $data[$i]['ship_state']=$shippingAddress['region'];
                    $data[$i]['ship_zip']=$shippingAddress['postcode'];
                    $data[$i]['ship_country']=$shippingAddress['country_id'];
                    $data[$i]['bill_first_name']=$billingAddress['firstname'];
                    $data[$i]['bill_last_name']=$billingAddress['lastname'];
                    $data[$i]['bill_street1']=$billingAddress['street'];
                    $data[$i]['bill_street2']='';
                    $data[$i]['bill_city']=$billingAddress['city'];
                    $data[$i]['bill_state']=$billingAddress['region'];
                    $data[$i]['bill_zip']=$billingAddress['postcode'];
                    $data[$i]['bill_country']=$billingAddress['country_id'];
                    $data[$i]['instructions']=$getorderdata['customer_note'];
                    $data[$i]['pointdiscount']='';
                    $data[$i]['giftwrap']=$getorderdata['gift_message_id'];
                    $data[$i]['taxrate']=$itemDetail['tax_percent'];
                    $data[$i]['bill_company']=$billingAddress['company'];
                    $data[$i]['ship_company']=$shippingAddress['company'];
                    $data[$i]['email']=$billingAddress['email'];
                    $data[$i]['shipping_method']=$getorderdata['shipping_method'];
                    $data[$i]['realtime_confirmation']=$getorderdata['paypal_ipn_customer_notified'];
                    $data[$i]['grand_total']=$getorderdata['grand_total'];
                    $data[$i]['phone_number']=$billingAddress['telephone'];
                    $data[$i]['weight']=$itemDetail['weight'];
                    fputcsv($handle,$data[$i], "\t");
                }
            }
            fclose($handle);
        }
        return $lastOrder;
    }
}
?>
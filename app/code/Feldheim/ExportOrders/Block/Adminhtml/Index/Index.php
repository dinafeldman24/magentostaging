<?php
namespace Feldheim\ExportOrders\Block\Adminhtml\Index;


class Index extends \Magento\Backend\Block\Widget\Container
{
    protected $_checkoutSession;
    public function __construct(\Magento\Backend\Block\Widget\Context $context,\Magento\Checkout\Model\Session $checkoutSession, array $data = [])
    {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context,$data);
    }


    public function updateCancelTime(){

        return $this->formKey->getFormKey();
    }

    public function exportTheseOrders($exportOrder, $lastOrder){
        $data=array();

        if ($exportOrder==$lastOrder || $exportOrder > $lastOrder){
            echo "<div style='color:#f00;'>All orders were already exported, change last order exported number to re-export</div>";
        }
        else{
            $heading = array("order_id","date_placed","username","tax","shipping_cost","card_type_string","card_number","card_ccid","card_exp_month","card_exp_year","card_holder","coupondiscount2","Item #","retail_price","discount","quantity","unit_price","ship_first_name","ship_last_name","ship_street1","ship_street2","ship_city","ship_state","ship_zip","ship_country","bill_first_name","bill_last_name","bill_street1","bill_street2","bill_city","bill_state","bill_zip","bill_country","special_instructions","pointdiscount","giftwrap","taxrate","bill_company","ship_company","email","shipping_method","realtime_confirmation","grand_total","phone_number","weight");
            $today=date("mdY-his");
            $outputFile = $_SERVER['DOCUMENT_ROOT']."/custom/feldheim/".$today."_ORDERS.csv";
            $handle = fopen($outputFile, "w");
            fputcsv($handle,$heading, "\t");
             $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
            $i=$exportOrder+1;
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('mgsg_gift_message');
            for (;$i<=$lastOrder;$i++){
                 $message='';
                $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection();
                $orderData = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($i);
                //$orderData = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId(100052726);
                $gift = $orderData->getGiftMessageId();
                if ($gift > 0){
                    $sql="SELECT * FROM ".$tableName." WHERE gift_message_id = ".$gift;
                    $result = $connection->fetchAll($sql);
                    $message="Sender: ".$result[0]['sender'] .", recipient: ".$result[0]['recipient']. ", Message: ".$result[0]['message'];
                }
                if (!$orderData) continue;
                $getorderdata = $orderData->getData();
                $billingAddress = $orderData->getBillingAddress();
                $shippingAddress = $orderData->getShippingAddress();
                $payment = $orderData->getPayment();
                if ($payment){
                    $method = $payment->getMethodInstance();
                    $methodTitle = $method->getTitle();
                    if ($methodTitle=='PayPal Express Checkout')$methodTitle='PAYP';
                    $ccType=$payment->getCcType();
                    if ($ccType=='PayPal Express Checkout') $ccType='PAYP';
                    if ($ccType=='VI') $ccType='VISA';
                    if ($ccType=='AE') $ccType='AMEX';
                    $expmonth=$payment->getAdditionalInformation('cc_exp_month');
                    $expyear=$payment->getAdditionalInformation('cc_exp_year');   
                    $cclast4=$payment->getCcLast4();  
                }
                $orderItems = $orderData->getAllVisibleItems();
                foreach($orderItems as $orderItem){
                    $itemDetail=$orderItem->getData();
                    $data[$i]['order_id']=$i;
                    $formatted_date=date("m/d/Y", strtotime($getorderdata['created_at']));
                    $data[$i]['date_placed']=$formatted_date;
                    $data[$i]['username']=$billingAddress['email'];
                    $data[$i]['tax']=$getorderdata['tax_amount'];
                    $data[$i]['shipping_cost']=$getorderdata['shipping_amount'];
                    $data[$i]['card_type_string']= ($ccType) ? $ccType : $methodTitle;
                    $data[$i]['card_number']=$cclast4;
                    $data[$i]['card_ccid']='';
                    $data[$i]['card_exp_month']=$expmonth;
                    $data[$i]['card_exp_year']=$expyear;
                    $data[$i]['card_holder']='';
                    $data[$i]['coupondiscount2']=$itemDetail['discount_amount'];
                    $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($itemDetail['product_id']);
                    $sku=$_product->getData('sku');
                    $data[$i]['item']=$sku;
                    $data[$i]['retail_price']=$itemDetail['original_price'];
                    if ($sku =='gift-certificate-test'){
                        $data[$i]['retail_price']=$itemDetail['price'];
                    } 
                    $data[$i]['discount']=$itemDetail['original_price']-$itemDetail['price'];
                    $data[$i]['quantity']=$itemDetail['qty_ordered'];
                    $data[$i]['unit_price']=$itemDetail['price'];
                    $data[$i]['ship_first_name']=$shippingAddress['firstname'];
                    $data[$i]['ship_last_name']=$shippingAddress['lastname'];
                    $shipStreet = str_replace(array("\r\n", "\n\r", "\n", "\r"), ', ', $shippingAddress['street']);
                    $data[$i]['ship_street1']=$shipStreet;
                    $data[$i]['ship_street2']='';
                    $data[$i]['ship_city']=$shippingAddress['city'];
                    $state_full=$shippingAddress['region'];
                    switch($state_full) {
        case "District of Columbia":
            $state = "DC";
            break;
        case "Alaska":
            $state = "AK";
            break;
        case "Alabama":
            $state = "AL";
            break;
        case "Arkansas":
            $state = "AR";
            break;
        case "Arizona":
            $state = "AZ";
            break;
        case "California":
            $state = "CA";
            break;
        case "Colorado":
            $state = "CO";
            break;
        case "Connecticut":
            $state = "CT";
            break;
        case "Delaware":
            $state = "DE";
            break;
        case "Florida":
            $state = "FL";
            break;
        case "Georgia":
            $state = "GA";
            break;
        case "Hawaii":
            $state = "HI";
            break;
        case "Iowa":
            $state = "IA";
            break;
        case "Idaho":
            $state = "ID";
            break;
        case "Illinois":
            $state = "IL";
            break;
        case "Indiana":
            $state = "IN";
            break;
        case "Kansas":
            $state = "KS";
            break;
        case "Kentucky":
            $state = "KY";
            break;
        case "Louisiana":
            $state = "LA";
            break;
        case "Massachusetts":
            $state = "MA";
            break;
        case "Maryland":
            $state = "MD";
            break;
        case "Maine":
            $state = "ME";
            break;
        case "Michigan":
            $state = "MI";
            break;
        case "Minnesota":
            $state = "MN";
            break;
        case "Missouri":
            $state = "MO";
            break;
        case "Mississippi":
            $state = "MS";
            break;
        case "Montana":
            $state = "MT";
            break;
        case "North Carolina":
            $state = "NC";
            break;
        case "North Dakota":
            $state = "ND";
            break;
        case "Nebraska":
            $state = "NE";
            break;
        case "New Hampshire":
            $state = "NH";
            break;
        case "New Jersey":
            $state = "NJ";
            break;
        case "New Mexico":
            $state = "NM";
            break;
        case "Nevada":
            $state = "NV";
            break;
        case "New York":
            $state = "NY";
            break;
        case "Ohio":
            $state = "OH";
            break;
        case "Oklahoma":
            $state = "OK";
            break;
        case "Oregon":
            $state = "OR";
            break;
        case "Pennsylvania":
            $state = "PA";
            break;
        case "Rhode Island":
            $state = "RI";
            break;
        case "South Carolina":
            $state = "SC";
            break;
        case "South Dakota":
            $state = "SD";
            break;
        case "Tennessee":
            $state = "TN";
            break;
        case "Texas":
            $state = "TX";
            break;
        case "Utah":
            $state = "UT";
            break;
        case "Virginia":
            $state = "VA";
            break;
        case "Vermont":
            $state = "VT";
            break;
        case "Washington":
            $state = "WA";
            break;
        case "Wisconsin":
            $state = "WI";
            break;
        case "West Virginia":
            $state = "WV";
            break;
        case "Wyoming":
            $state = "WY";
            break;
        default  : $state = "";
            break;    
    }
                    $data[$i]['ship_state']=$state;
                    $data[$i]['ship_zip']=$shippingAddress['postcode'];
                    $data[$i]['ship_country']=$shippingAddress['country_id'];
                    $data[$i]['bill_first_name']=$billingAddress['firstname'];
                    $data[$i]['bill_last_name']=$billingAddress['lastname'];
                    $billStreet = str_replace(array("\r\n", "\n\r", "\n", "\r"), ', ', $billingAddress['street']);
                    $data[$i]['bill_street1']=$billStreet;
                    $data[$i]['bill_street2']='';
                    $data[$i]['bill_city']=$billingAddress['city'];
                    $state_full=$billingAddress['region'];
                     switch($state_full) {
        case "District of Columbia":
            $state = "DC";
            break;
        case "Alaska":
            $state = "AK";
            break;
        case "Alabama":
            $state = "AL";
            break;
        case "Arkansas":
            $state = "AR";
            break;
        case "Arizona":
            $state = "AZ";
            break;
        case "California":
            $state = "CA";
            break;
        case "Colorado":
            $state = "CO";
            break;
        case "Connecticut":
            $state = "CT";
            break;
        case "Delaware":
            $state = "DE";
            break;
        case "Florida":
            $state = "FL";
            break;
        case "Georgia":
            $state = "GA";
            break;
        case "Hawaii":
            $state = "HI";
            break;
        case "Iowa":
            $state = "IA";
            break;
        case "Idaho":
            $state = "ID";
            break;
        case "Illinois":
            $state = "IL";
            break;
        case "Indiana":
            $state = "IN";
            break;
        case "Kansas":
            $state = "KS";
            break;
        case "Kentucky":
            $state = "KY";
            break;
        case "Louisiana":
            $state = "LA";
            break;
        case "Massachusetts":
            $state = "MA";
            break;
        case "Maryland":
            $state = "MD";
            break;
        case "Maine":
            $state = "ME";
            break;
        case "Michigan":
            $state = "MI";
            break;
        case "Minnesota":
            $state = "MN";
            break;
        case "Missouri":
            $state = "MO";
            break;
        case "Mississippi":
            $state = "MS";
            break;
        case "Montana":
            $state = "MT";
            break;
        case "North Carolina":
            $state = "NC";
            break;
        case "North Dakota":
            $state = "ND";
            break;
        case "Nebraska":
            $state = "NE";
            break;
        case "New Hampshire":
            $state = "NH";
            break;
        case "New Jersey":
            $state = "NJ";
            break;
        case "New Mexico":
            $state = "NM";
            break;
        case "Nevada":
            $state = "NV";
            break;
        case "New York":
            $state = "NY";
            break;
        case "Ohio":
            $state = "OH";
            break;
        case "Oklahoma":
            $state = "OK";
            break;
        case "Oregon":
            $state = "OR";
            break;
        case "Pennsylvania":
            $state = "PA";
            break;
        case "Rhode Island":
            $state = "RI";
            break;
        case "South Carolina":
            $state = "SC";
            break;
        case "South Dakota":
            $state = "SD";
            break;
        case "Tennessee":
            $state = "TN";
            break;
        case "Texas":
            $state = "TX";
            break;
        case "Utah":
            $state = "UT";
            break;
        case "Virginia":
            $state = "VA";
            break;
        case "Vermont":
            $state = "VT";
            break;
        case "Washington":
            $state = "WA";
            break;
        case "Wisconsin":
            $state = "WI";
            break;
        case "West Virginia":
            $state = "WV";
            break;
        case "Wyoming":
            $state = "WY";
            break;
        default  : $state = "";
            break;    
    }
                    $data[$i]['bill_state']=$state;
                    $data[$i]['bill_zip']=$billingAddress['postcode'];
                    $data[$i]['bill_country']=$billingAddress['country_id'];
                    $data[$i]['special_instructions']=$message;
                   // $data[$i]['instructions']=$getorderdata['customer_note'];
                    $data[$i]['pointdiscount']='';
                    $data[$i]['giftwrap']=$getorderdata['gift_message_id'];
                    $data[$i]['taxrate']=$itemDetail['tax_percent'];
                    $data[$i]['bill_company']=$billingAddress['company'];
                    $data[$i]['ship_company']=$shippingAddress['company'];
                    $data[$i]['email']=$billingAddress['email'];
                    $shipMethod='';
                    if ($getorderdata['shipping_method']=='flatrate_flatrate' )
                            $shipMethod='PM';
                    if ($getorderdata['shipping_method']=='matrixrate_matrixrate_1' || $getorderdata['shipping_method']=='matrixrate_matrixrate_8' || $getorderdata['shipping_method']=='matrixrate_matrixrate_9'  || $getorderdata['shipping_method']=='freeshipping_freeshipping')
                            $shipMethod='PP';       
                    if ($getorderdata['shipping_method']=='tablerate_bestway')
                            $shipMethod='UPS';   
                    if ($getorderdata['shipping_method']=='usps_INT_15')
                            $shipMethod='FCPI';                        
                    if ($getorderdata['shipping_method']=='usps_INT_2')
                            $shipMethod='GLOB';                             
                    $data[$i]['shipping_method']=$shipMethod;
                    $data[$i]['realtime_confirmation']=$getorderdata['paypal_ipn_customer_notified'];
                    $data[$i]['grand_total']=$getorderdata['grand_total'];
                    $data[$i]['phone_number']=$billingAddress['telephone'];
                    $data[$i]['weight']=$itemDetail['weight'];
                    fputcsv($handle,$data[$i], "\t");
                }
            }
            fclose($handle);
            echo "<p><br>Report was exported to ".$outputFile."</p>";
        }
        return $lastOrder;
    }

    public function getRealOrderId()
    {
        $order =  $this->_checkoutSession->getLastRealOrder();
        $order=$this->_checkoutSession->getLastRealOrder();
        $lastorderId=$order->getId();

        return $lastorderId;
    }


}
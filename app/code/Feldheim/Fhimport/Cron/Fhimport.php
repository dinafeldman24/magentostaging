<?php

namespace Feldheim\Fhimport\Cron;
 
class Fhimport
{
	protected $logger;
	protected $productRepository;
  protected $state;
	
	public function __construct(
	 	\Psr\Log\LoggerInterface $loggerInterface,
	  \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
	) {
		$this->logger = $loggerInterface;
	  $this->productRepository = $productRepository;
	}
 
	public function execute() {

		//test command line
        //php bin/magento cron:run --group="feldheim_exportorders_cron_group"
	$this->logger->debug('Feldheim\Fhimport\Cron\Fhimport');
// MAGENTO START
  $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
  $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
  $stockRegistry= $objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');
  $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
  $File = $directory->getRoot() .'/custom/sync/inventory.txt';
	$Handle = fopen($File,"r");
	$Content = fread ($Handle,filesize ($File));
	fclose($Handle);
		
	 $list = explode("\n", $Content);
	 array_shift($list);
	 $i=0;
   $message = '';
   $count   = 1;
	 foreach ($list as $row)
	 {
	   set_time_limit(0);
  	 $onerow=explode("\t",$row);
     $fixedrow=str_replace('"', '', $onerow);
	 $notfound=0;
	 $found=0;
     try {
		     //$product = $productRepository->get($fixedrow[0]);
			 $product = $productRepository->get($fixedrow[0],false, 1, false);
		 }
		 catch (\Magento\Framework\Exception\NoSuchEntityException $e){
		   try {
		   	    $sku="0".$fixedrow[0];
		   		$product = $productRepository->get($sku,false, 1, false);
		   }
		   	catch (\Magento\Framework\Exception\NoSuchEntityException $e){
           	$product = false;
		   	echo "can't find product sku is " . $fixedrow[0] . "\n";
		  	$notfound++;
		  }
     } 
		// mail("dina.websites@gmail.com", "item", var_export($fixedrow,true));
		 if($product) {
	     if ($fixedrow[3]==0) continue;		
         if ($fixedrow[0]==1635)continue;		 
       if ($fixedrow[4]=='yes'){ 		 
            $stockItem = $stockRegistry->getStockItemBySku($fixedrow[0]);
            $stockItem->setQty(1000);
            $stockItem->setIsInStock(true);
            $stockRegistry->updateStockItemBySku($fixedrow[0], $stockItem);
			$product->setQuantityAndStockStatus(['qty' => 1000, 'is_in_stock' => 1]);
			echo "Setting stock of sku " . $fixedrow[0] ."\n";
			 }	 
			 else{	 
            $stockItem = $stockRegistry->getStockItemBySku($fixedrow[0]);
            $stockItem->setQty(0);
            $stockItem->setIsInStock(false);
            $stockRegistry->updateStockItemBySku($fixedrow[0], $stockItem);
			$product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => 0]);
			 }	 
			if(strtoupper($fixedrow[6])=='OBSOLETE'){
			    echo "Setting status to disabled now sku is " . $fixedrow[0] ."\n";
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
			 }		
			 else 
            { 
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
			 }		
			 if ($fixedrow[3]>0)
			 {
			    $product->setPrice($fixedrow[3]);
			 }
			 
			 if ($fixedrow[7] > 0 ){
			 		$product->setWeight($fixedrow[7]);
			 }
			 if ($fixedrow[5]){
				 $date = date("Y/m/d", strtotime($fixedrow[5]));
                 $product->setCustomAttribute('back_in_stock_date', $date);
			 }
			$product->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);
       		$productRepository->save($product);
			
			echo "Product Updated ".$product->getId()." sku is " . $fixedrow[0] . "\n";
            $found++;       
		 }


   }
	 
	 mail("dina.websites@gmail.com", "Feldheim Import done", "Read ".$found ." items");
	 mail("dina.websites@gmail.com", "Feldheim Import done", "Can't find ".$notfound ." items");

	}
}
<?php

namespace MageCloud\Email\Plugin\Email\Model;

class AbstractTemplate
{
    /**
     * Package area
     *
     * @var string
     */
    private $area;

    /**
     * @var \Magento\Email\Model\Template\Config
     */
    private $emailConfig;

    /**
     * AbstractTemplate constructor.
     * @param \Magento\Email\Model\Template\Config $emailConfig
     */
    public function __construct(
        \Magento\Email\Model\Template\Config $emailConfig
    ) {
        $this->emailConfig = $emailConfig;
    }

    public function aroundSetForcedArea(\Magento\Email\Model\AbstractTemplate $subject, \Closure $proceed, $templateId)
    {
        $this->area = $subject->getData('area');
        if (!isset($this->area)) {
            $this->area = $this->emailConfig->getTemplateArea($templateId);
        }
        return $subject;
    }
}
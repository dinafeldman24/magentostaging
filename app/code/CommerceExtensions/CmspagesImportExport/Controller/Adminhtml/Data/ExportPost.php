<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CmspagesImportExport\Controller\Adminhtml\Data;

use Magento\Framework\App\Resource;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportPost extends \CommerceExtensions\CmspagesImportExport\Controller\Adminhtml\Data
{ 
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
	/**
     * Write connection adapter
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;
	protected $fileFactory;
    protected $storeManager;
    protected $cmsPage;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Cms\Model\Page $cmsPage
    ) {
        $this->fileFactory = $fileFactory;
        $this->_storeManager = $storeManager;
        $this->_cmsPage = $cmsPage;
        parent::__construct($context,$fileFactory);
    }
	
    public function execute()
    {
		$params = $this->getRequest()->getParams();
		$_resource = $this->_objectManager->create('Magento\Framework\App\ResourceConnection');
		$cms_page = $_resource->getTableName('cms_page');
		$cms_page_store = $_resource->getTableName('cms_page_store');
		
		if($params['export_delimiter'] != "") {
			$delimiter = $params['export_delimiter'];
		} else {
			$delimiter = ",";
		}
		if($params['export_enclose'] != "") {
			$enclose = $params['export_enclose'];
		} else {
			$enclose = "\"";
		}
		
        /** start csv content and set template */
        $headers = new \Magento\Framework\DataObject(
            [
                'created_at' => __('created_at'),
                'page_title' => __('page_title'),
                'url_key' => __('url_key'),
                'storeview' => __('storeview'),
                'status' => __('status'),
                'content_heading' => __('content_heading'),
                'content' => __('content'),
                'layout' => __('layout'),
                'meta_keyword' => __('meta_keyword'),
                'meta_description' => __('meta_description'),
                'layout_update_xml' => __('layout_update_xml'),
                'custom_theme' => __('custom_theme'),
                'custom_root_template' => __('custom_root_template'),
                'custom_layout_update_xml' => __('custom_layout_update_xml'),
                'custom_theme_from' => __('custom_theme_from'),
                'custom_theme_to' => __('custom_theme_to'),
                'sort_order' => __('sort_order')
            ]
        );
		
        $template = ''.$enclose.'{{created_at}}'.$enclose.''.$delimiter.''.$enclose.'{{page_title}}'.$enclose.''.$delimiter.''.$enclose.'{{url_key}}'.$enclose.''.$delimiter.''.$enclose.'{{storeview}}'.$enclose.''.$delimiter.''.$enclose.'{{status}}'.$enclose.''.$delimiter.''.$enclose.'{{content_heading}}'.$enclose.''.$delimiter.''.$enclose.'{{content}}'.$enclose.''.$delimiter.''.$enclose.'{{layout}}'.$enclose.''.$delimiter.''.$enclose.'{{meta_keyword}}'.$enclose.''.$delimiter.''.$enclose.'{{meta_description}}'.$enclose.''.$delimiter.''.$enclose.'{{layout_update_xml}}'.$enclose.''.$delimiter.''.$enclose.'{{custom_theme}}'.$enclose.''.$delimiter.''.$enclose.'{{custom_root_template}}'.$enclose.''.$delimiter.''.$enclose.'{{custom_layout_update_xml}}'.$enclose.''.$delimiter.''.$enclose.'{{custom_theme_from}}'.$enclose.''.$delimiter.''.$enclose.'{{custom_theme_to}}'.$enclose.''.$delimiter.''.$enclose.'{{sort_order}}'.$enclose.'';
        $content = $headers->toString($template);
        $storeTemplate = [];
        $content .= "\n";
        
		$connection = $_resource->getConnection();
		
		if($this->getEdition() == "Community") { 
			if($params['export_by_store_id'] != "" && is_numeric($params['export_by_store_id'])) {
			 $query = "SELECT CMSpage.page_id, CMSpage.title, CMSpage.meta_keywords, CMSpage.meta_description, CMSpage.identifier, CMSpage.content_heading, CMSpage.content, CMSpage.page_layout, CMSpage.creation_time, CMSpage.update_time, CMSpage.is_active, CMSpage.layout_update_xml, CMSpage.custom_theme, CMSpage.custom_root_template, CMSpage.custom_layout_update_xml, CMSpage.custom_theme_from, CMSpage.custom_theme_to, CMSpage.sort_order FROM ".$cms_page." AS CMSpage INNER JOIN ".$cms_page_store." AS CMSpageStore ON CMSpageStore.page_id = CMSpage.page_id WHERE CMSpageStore.store_id = '".$params['export_by_store_id']."'";
			 } else {
			 $query = "SELECT CMSpage.page_id, CMSpage.title, CMSpage.meta_keywords, CMSpage.meta_description, CMSpage.identifier, CMSpage.content_heading, CMSpage.content, CMSpage.page_layout, CMSpage.creation_time, CMSpage.update_time, CMSpage.is_active, CMSpage.layout_update_xml, CMSpage.custom_theme, CMSpage.custom_root_template, CMSpage.custom_layout_update_xml, CMSpage.custom_theme_from, CMSpage.custom_theme_to, CMSpage.sort_order FROM ".$cms_page." AS CMSpage";
			 }
		} else {
			if($params['export_by_store_id'] != "" && is_numeric($params['export_by_store_id'])) {
			 $query = "SELECT CMSpage.row_id, CMSpage.page_id, CMSpage.title, CMSpage.meta_keywords, CMSpage.meta_description, CMSpage.identifier, CMSpage.content_heading, CMSpage.content, CMSpage.page_layout, CMSpage.creation_time, CMSpage.update_time, CMSpage.is_active, CMSpage.layout_update_xml, CMSpage.custom_theme, CMSpage.custom_root_template, CMSpage.custom_layout_update_xml, CMSpage.custom_theme_from, CMSpage.custom_theme_to, CMSpage.sort_order FROM ".$cms_page." CMSpage INNER JOIN ".$cms_page_store." CMSpageStore ON CMSpageStore.row_id = CMSpage.row_id WHERE CMSpageStore.store_id = '".$params['export_by_store_id']."'";
			} else {
			 $query = "SELECT CMSpage.row_id, CMSpage.page_id, CMSpage.title, CMSpage.meta_keywords, CMSpage.meta_description, CMSpage.identifier, CMSpage.content_heading, CMSpage.content, CMSpage.page_layout, CMSpage.creation_time, CMSpage.update_time, CMSpage.is_active, CMSpage.layout_update_xml, CMSpage.custom_theme, CMSpage.custom_root_template, CMSpage.custom_layout_update_xml, CMSpage.custom_theme_from, CMSpage.custom_theme_to, CMSpage.sort_order FROM ".$cms_page." AS CMSpage";
			}
		}
		$cmspagesCollection = $connection->fetchAll($query);
		foreach ($cmspagesCollection as $row) {
			$_cmsPageModel = $this->_cmsPage->load($row['page_id']);
			
			$storeTemplate["created_at"] = $row['creation_time'];
			$storeTemplate["page_title"] = $row['title'];
			$storeTemplate["url_key"] = $row['identifier'];
								
			$storeviewsforexport="";
			$finalstoreviewsforexport="";
			if($this->getEdition() == "Community") { 
				$select_store_ids_qry = "SELECT store_id FROM ".$cms_page_store." WHERE page_id = '".$row['page_id']."'";
			} else {
				$select_store_ids_qry = "SELECT store_id FROM ".$cms_page_store." WHERE row_id = '".$row['row_id']."'";
			}
			$storeidrows = $connection->fetchAll($select_store_ids_qry);
			foreach($storeidrows as $datastoreid)
			{ 
            	 $allstores = $this->_storeManager->getStores(true, true);
				 foreach ($allstores as $code => $store) {
						$storesIdCode[] = $code;
						if($store->getId() == $datastoreid['store_id']) {
							$storeviewsforexport .= $code . ",";
						}
				 }
			}
			$finalstoreviewsforexport = substr_replace($storeviewsforexport,"",-1);
			$storeTemplate["storeview"] = $finalstoreviewsforexport;
			if($row['is_active'] == "1") {
				$storeTemplate["status"] = "Enabled";
			} else {
				$storeTemplate["status"] = "Disabled";
			}
			if(!empty($row['content'])) {
				$storeTemplate["content"] = htmlspecialchars($row['content']);
			}
			$storeTemplate["layout"] = $row['page_layout'];
			$_cmsPageModel->addData($storeTemplate);
            $content .= $_cmsPageModel->toString($template) . "\n";
		}
		#exit;
        
        return $this->fileFactory->create('export_cms_pages.csv', $content, DirectoryList::VAR_DIR);
    }
	
	public function getEdition()
    {
		$magentoVersion = $this->_objectManager->create('Magento\Framework\App\ProductMetadataInterface');
        return $magentoVersion->getEdition();
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_CmspagesImportExport::import_export'
        );

    }
}

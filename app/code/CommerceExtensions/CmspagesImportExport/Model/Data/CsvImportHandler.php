<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CmspagesImportExport\Model\Data;

use Magento\Framework\App\ResourceConnection;

/**
 *  CSV Import Handler
 */
 
class CsvImportHandler
{ 
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
	
    /**
     * CMS Pages factory
     *
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $_cmsPageFactory;

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;
	
	protected $_stores;
	
    protected $storeManager;


    /**
     * @param \Magento\Store\Model\Resource\Store\Collection $storeCollection
     * @param \Magento\Cms\Model\PageFactory $cmsPageFactory
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        ResourceConnection $resource,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Cms\Model\PageFactory $cmsPageFactory,
		\Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        // prevent admin store from loading
        $this->_resource = $resource;
        $this->_objectManager = $objectManager;
        $this->_cmsPageFactory = $cmsPageFactory;
        $this->_storeManager = $storeManager;
        $this->csvProcessor = $csvProcessor;
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('created_at'),
            1 => __('page_title'),
            2 => __('url_key'),
            3 => __('storeview'),
            4 => __('status'),
            5 => __('content_heading'),
            6 => __('content'),
            7 => __('layout'),
            8 => __('meta_keyword'),
            9 => __('meta_description'),
            10 => __('layout_update_xml'),
            11 => __('custom_theme'),
            12 => __('custom_root_template'),
            13 => __('custom_layout_update_xml'),
            14 => __('custom_theme_from'),
            15 => __('custom_theme_to'),
            16 => __('sort_order')
        ];
    }

    /**
     * Import Data from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file, $params)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
		
		if($params['import_delimiter'] != "") { $this->csvProcessor->setDelimiter($params['import_delimiter']); }
		if($params['import_enclose'] != "") { $this->csvProcessor->setEnclosure($params['import_enclose']); }
		
        $RawData = $this->csvProcessor->getData($file['tmp_name']);
        // first row of file represents headers
        $fileFields = $RawData[0];
        $ratesData = $this->_filterData($fileFields, $RawData);
       
        foreach ($ratesData as $dataRow) {
            $this->_importCmsPages($dataRow, $params);
        }
    }


    /**
     * Filter data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $RawData
     * @param array $invalidFields assoc array of invalid file fields
     * @param array $validFields assoc array of valid file fields
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
     protected function _filterData(array $RawDataHeader, array $RawData)
    {
		$rowCount=0;
		$RawDataRows = array();
        foreach ($RawData as $rowIndex => $dataRow) {
			// skip headers
            if ($rowIndex == 0) {
				if(!in_array("page_title", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "page_title" NOT FOUND'));
				}
				if(!in_array("storeview", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "storeview" NOT FOUND'));
				}
				if(!in_array("url_key", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "url_key" NOT FOUND'));
				}
				if(!in_array("content", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "content" NOT FOUND'));
				}
				if(!in_array("status", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "status" NOT FOUND'));
				}
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
			/* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
				foreach ($dataRow as $rowIndex => $dataRowNew) {
					$RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
				}
			}
			$rowCount++;
        }
        return $RawDataRows;
    }

	
 	protected function _initStores ()
    {
        if (is_null($this->_stores)) {
            $this->_stores = $this->_storeManager->getStores(true, true);
            foreach ($this->_stores as $code => $store) {
                $this->_storesIdCode[$store->getId()] = $code;
            }
        }
    }
 		/**
     * Retrieve store object by code
     *
     * @param string $store
     * @return Mage_Core_Model_Store
     */
    public function getStoreByCode($store)
    {
        $this->_initStores();
        /**
         * In single store mode all data should be saved as default
         */
        if ($this->_storeManager->isSingleStoreMode()) {
			return $this->_storeManager->getStore(\Magento\Store\Model\Store::DEFAULT_STORE_ID);
        }

        if (isset($this->_stores[$store])) {
            return $this->_stores[$store];
        }
        return false;
    }
    /**
     * Import CMS Pages
     *
     * @param array $Data
     * @param array $storesCache cache of stores 
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _importCmsPages(array $Data, array $params)
    {
        	
			$cmsPageModel = $this->_cmsPageFactory->create();
			$cmsPageModel->load($Data['url_key'], 'identifier'); //will do update on cms page if matches
			#print_r($Data);
			if(isset($Data['created_at'])) { $cmsPageModel->setCreationTime($Data['created_at']); }
			if(isset($Data['page_title'])) { $cmsPageModel->setTitle($Data['page_title']); }
			if(isset($Data['url_key'])) { $cmsPageModel->setIdentifier($Data['url_key']); }
			if(isset($Data['storeview'])) { 
				// INSERTS CMS PAGE STORE DATA
				if($Data['storeview'] !="") {
					$finalstoreids=array();
					$delimiteddata = explode(',',$Data['storeview']);
					foreach ($delimiteddata as $individualstoreName) {
						if(is_numeric($individualstoreName)){
							$finalstoreids[] = $individualstoreName;
						} else {
							$store = $this->getStoreByCode($individualstoreName);
							if (is_object($store->getId())) {
								$finalstoreids[] = $store->getId();
							} else {
								throw new \Magento\Framework\Exception\LocalizedException(__('ERROR invalid "STOREVIEW" value: '. $individualstoreName));
							}
						}
					}
					$cmsPageModel->setStores($finalstoreids); 
				} else {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "STOREVIEW" is blank and it requires a value'));
				}
			}
			if(isset($Data['status'])) { 
				if($Data['status'] == "Enabled") { $cmsPageModel->setIsActive(1); } else { $cmsPageModel->setIsActive(0); } 
			}
			if(isset($Data['content_heading'])) { $cmsPageModel->setContentHeading($Data['content_heading']); }
			if(isset($Data['content'])) { $cmsPageModel->setContent(htmlspecialchars_decode($Data['content'])); }
			if(isset($Data['layout'])) { $cmsPageModel->setPageLayout($Data['layout']); }
			if(isset($Data['meta_keyword'])) { $cmsPageModel->setMetaKeywords($Data['meta_keyword']); }
			if(isset($Data['meta_description'])) { $cmsPageModel->setMetaDescription($Data['meta_description']); }
			if(isset($Data['layout_update_xml'])) { $cmsPageModel->setLayoutUpdateXml($Data['layout_update_xml']); }
			if(isset($Data['custom_theme'])) { $cmsPageModel->setCustomTheme($Data['custom_theme']); }
			if(isset($Data['custom_root_template'])) { $cmsPageModel->setCustomRootTemplate($Data['custom_root_template']); }
			if(isset($Data['custom_layout_update_xml'])) { $cmsPageModel->setCustomLayoutUpdateXml($Data['custom_layout_update_xml']); }
			if(isset($Data['custom_theme_from'])) { $cmsPageModel->setCustomThemeFrom($Data['custom_theme_from']); }
			if(isset($Data['custom_theme_to'])) { $cmsPageModel->setCustomThemeTo($Data['custom_theme_to']); }
			if(isset($Data['sort_order'])) { $cmsPageModel->setSortOrder($Data['sort_order']); }
			
			try {
				$cmsPageModel->save();
			} catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
			}
    }
}
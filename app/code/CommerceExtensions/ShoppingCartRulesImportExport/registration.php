<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'CommerceExtensions_ShoppingCartRulesImportExport',
    __DIR__
);
<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ShoppingCartRulesImportExport\Controller\Adminhtml\Data;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportPost extends \CommerceExtensions\ShoppingCartRulesImportExport\Controller\Adminhtml\Data
{
    /**
     * Export action from import/export categories
     *
     * @return ResponseInterface
     */
	 public function unserialize($string)
	{
		if($this->is_serialized($string))
		{
			$string = $this->serialize($string);
		}
		$result = json_decode($string, true);
		if (json_last_error() !== JSON_ERROR_NONE) {
			 throw new \InvalidArgumentException('Unable to unserialize value.');
	
		}
		return $result;
	}
    public function execute()
    {
        /** start csv content and set template */
        $dataExport = new \Magento\Framework\DataObject();
		$params = $this->getRequest()->getParams();
	    $resource = $this->_objectManager->create('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$_catalog_category_product = $resource->getTableName('catalog_category_product');
		
		if($params['export_delimiter'] != "") {
			$delimiter = $params['export_delimiter'];
		} else {
			$delimiter = ",";
		}
		if($params['export_enclose'] != "") {
			$enclose = $params['export_enclose'];
		} else {
			$enclose = "\"";
		}
		
        $headers = new \Magento\Framework\DataObject(
            [
                'name' => __('name'),
                'label' => __('label'),
                'code' => __('code'),
                'conditions_aggregator' => __('conditions_aggregator'),
                'conditions' => __('conditions'),
                'actions_aggregator' => __('actions_aggregator'),
                'actions' => __('actions'),
                'usage_limit' => __('usage_limit'),
                'expiration_date' => __('expiration_date'),
                'description' => __('description'),
                'from_date' => __('from_date'),
                'to_date' => __('to_date'),
                'uses_per_customer' => __('uses_per_customer'),
                'customer_group_ids' => __('customer_group_ids'),
                'is_active' => __('is_active'),
                'is_primary' => __('is_primary'),
                'stop_rules_processing' => __('stop_rules_processing'),
                'is_advanced' => __('is_advanced'),
                'product_ids' => __('product_ids'),
                'sort_order' => __('sort_order'),
                'simple_action' => __('simple_action'),
                'discount_amount' => __('discount_amount'),
                'discount_qty' => __('discount_qty'),
                'discount_step' => __('discount_step'),
                'simple_free_shipping' => __('simple_free_shipping'),
                'apply_to_shipping' => __('apply_to_shipping'),
                'times_used' => __('times_used'),
                'is_rss' => __('is_rss'),
                'coupon_type' => __('coupon_type'),
                'use_auto_generation' => __('use_auto_generation'),
                'type' => __('type'),
                'coupon_times_used' => __('coupon_times_used'),
                'website_ids' => __('website_ids')
            ]
        );
        $template = ''.$enclose.'{{name}}'.$enclose.''.$delimiter.''.$enclose.'{{label}}'.$enclose.''.$delimiter.''.$enclose.'{{code}}'.$enclose.''.$delimiter.''.$enclose.'{{conditions_aggregator}}'.$enclose.''.$delimiter.''.$enclose.'{{conditions}}'.$enclose.''.$delimiter.''.$enclose.'{{actions_aggregator}}'.$enclose.''.$delimiter.''.$enclose.'{{actions}}'.$enclose.''.$delimiter.''.$enclose.'{{usage_limit}}'.$enclose.''.$delimiter.''.$enclose.'{{expiration_date}}'.$enclose.''.$delimiter.''.$enclose.'{{description}}'.$enclose.''.$delimiter.''.$enclose.'{{from_date}}'.$enclose.''.$delimiter.''.$enclose.'{{to_date}}'.$enclose.''.$delimiter.''.$enclose.'{{uses_per_customer}}'.$enclose.''.$delimiter.''.$enclose.'{{customer_group_ids}}'.$enclose.''.$delimiter.''.$enclose.'{{is_active}}'.$enclose.''.$delimiter.''.$enclose.'{{is_primary}}'.$enclose.''.$delimiter.''.$enclose.'{{stop_rules_processing}}'.$enclose.''.$delimiter.''.$enclose.'{{is_advanced}}'.$enclose.''.$delimiter.''.$enclose.'{{product_ids}}'.$enclose.''.$delimiter.''.$enclose.'{{sort_order}}'.$enclose.''.$delimiter.''.$enclose.'{{simple_action}}'.$enclose.''.$delimiter.''.$enclose.'{{discount_amount}}'.$enclose.''.$delimiter.''.$enclose.'{{discount_qty}}'.$enclose.''.$delimiter.''.$enclose.'{{discount_step}}'.$enclose.''.$delimiter.''.$enclose.'{{simple_free_shipping}}'.$enclose.''.$delimiter.''.$enclose.'{{apply_to_shipping}}'.$enclose.''.$delimiter.''.$enclose.'{{times_used}}'.$enclose.''.$delimiter.''.$enclose.'{{is_rss}}'.$enclose.''.$delimiter.''.$enclose.'{{coupon_type}}'.$enclose.''.$delimiter.''.$enclose.'{{use_auto_generation}}'.$enclose.''.$delimiter.''.$enclose.'{{type}}'.$enclose.''.$delimiter.''.$enclose.'{{coupon_times_used}}'.$enclose.''.$delimiter.''.$enclose.'{{website_ids}}'.$enclose.'';
      $content = $headers->toString($template);
	  $content .= "\n";
		
	  $connection = $resource->getConnection();
	  $prefix = $resource->getTableName('salesrule') ; //gives table name with prefix
	  $prefix1 = $resource->getTableName('salesrule_coupon');
	  $salesrule_label = $resource->getTableName('salesrule_label');
	  $salesrule_customer_group = $resource->getTableName('salesrule_customer_group');
	  $salesrule_website = $resource->getTableName('salesrule_website');
	  
	  $sql = "SELECT ".$prefix.".rule_id, ".$prefix.".name, ".$prefix.".description, ".$prefix.".from_date, ".$prefix.".to_date, ".$prefix.".uses_per_customer, ".$prefix.".is_active, ".$prefix.".conditions_serialized, ".$prefix.".actions_serialized, ".$prefix.".stop_rules_processing, ".$prefix.".is_advanced, ".$prefix.".product_ids, ".$prefix.".sort_order, ".$prefix.".simple_action, ".$prefix.".discount_amount, ".$prefix.".discount_qty, ".$prefix.".discount_step, ".$prefix.".simple_free_shipping, ".$prefix.".apply_to_shipping, ".$prefix.".times_used, ".$prefix.".is_rss, ".$prefix.".coupon_type, ".$prefix.".use_auto_generation, ".$prefix1.".code, ".$prefix1.".usage_limit, ".$prefix1.".expiration_date, ".$prefix1.".is_primary, ".$prefix1.".type, ".$prefix.".times_used AS coupon_times_used FROM ".$prefix." LEFT JOIN ".$prefix1." ON ".$prefix.".rule_id = ".$prefix1.".rule_id";     
						 
	   $rows = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
	   $row = array();
	   $final_arr = array();
	   foreach($rows as $data) {
	   
			$storeTemplate["name"] = $data['name'];
			   
			$finallabelsfromoptions="";
			
			$select_qry3 = "SELECT store_id, label FROM ".$salesrule_label." WHERE rule_id = '".$data['rule_id']."'";
			$rows3 = $resource->getconnection()->fetchAll($select_qry3);
			
			foreach($rows3 as $data3) { 
				$finallabelsfromoptions .= $data3['store_id'] . "~" . $data3['label'] . "^";
			}
			
			$okcleanedfinallabels = substr_replace($finallabelsfromoptions,"",-1);
			$storeTemplate["label"] = $okcleanedfinallabels;
					
		if (isset($data['coupon_code'])) {
			$storeTemplate["code"] = $data['coupon_code'];
		} else {
			$storeTemplate["code"] = $data['code'];
		}
								
		// conditions_serialized   
		$finalconditionsvalues="";
		$conditions_serialized = (string)$data['conditions_serialized'];
		
		if($this->getVersion() > '2.2.0') {
	    	$jsonSerializer = $this->_objectManager->create('Magento\Framework\Serialize\Serializer\Json');
			$conditions_unserialized = $jsonSerializer->unserialize($conditions_serialized);
		} else {
			$conditions_unserialized = unserialize($data['conditions_serialized']);
		}
		
		$storeTemplate["conditions_aggregator"] = $conditions_unserialized["aggregator"];
		
		if(isset($conditions_unserialized["conditions"])){
			#echo "<pre/>" ;
			#print_r($conditions_unserialized);
			#$recordcountconditions=0;
			#echo "NAME: " . $data['name'] . "<br/>";
			foreach($conditions_unserialized["conditions"] as $conditions_data){ 
				#echo  "<pre/>" ;
				#print_r($conditions_data);
				#echo "ID : " . $conditions_data['type'];
				$finalconditionsvalues .= $conditions_data['type']."~".$conditions_data['attribute']."~".$conditions_data['operator']."~".$conditions_data['value']."^";
				
				if($conditions_data['type'] == "Magento\SalesRule\Model\Rule\Condition\Address") {
				
				} else if($conditions_data['type'] == "Magento\SalesRule\Model\Rule\Condition\Product\Subselect") {	
				
					
					foreach($conditions_data['conditions'] as $sub_conditions_data){ 
						$finalconditionsvalues .= $sub_conditions_data['type'] . "~" . $sub_conditions_data['attribute'] . "~" . $sub_conditions_data['operator'] . "~" . $sub_conditions_data['value'] . "^";
					}
					/*
					if(isset($conditions_data['conditions'][$recordcountconditions]['type'])) {
						$finalconditionsvalues .= $conditions_data['conditions'][$recordcountconditions]['type'] . "~" . $conditions_data['conditions'][$recordcountconditions]['attribute'] . "~" . $conditions_data['conditions'][$recordcountconditions]['operator'] . "~" . $conditions_data['conditions'][$recordcountconditions]['value'] . "^";
						$recordcountconditions++;
					}
					*/
				}
			}
			$okcleanedfinalvalues = substr_replace($finalconditionsvalues,"",-1);
			$storeTemplate["conditions"] = $okcleanedfinalvalues;
		} else {
			$storeTemplate["conditions"] = '';
		}
			#  echo "<pre>";
			#	print_r($row["conditions"]);
					
			//  actions_serialized
			$finalactionsvalues="";
			$actions_serialized = (string)$data['actions_serialized'];
			if($this->getVersion() > '2.2.0') {
				//$jsonSerializer = $this->_objectManager->create('Magento\Framework\Serialize\Serializer\Json');
				$actions_unserialized = $jsonSerializer->unserialize($actions_serialized);
			} else {
				$actions_unserialized = unserialize($data['actions_serialized']);
			}
			$storeTemplate["actions_aggregator"] = $actions_unserialized["aggregator"];

			if(isset($actions_unserialized["conditions"])){
				foreach($actions_unserialized["conditions"] as $actions_data) { 
					$finalactionsvalues .= $actions_data['attribute'] . "~" . $actions_data['operator'] . "~" . $actions_data['value'] . "^";
				}
				$okcleanedfinalvalues = substr_replace($finalactionsvalues,"",-1);
				$storeTemplate["actions"] = $okcleanedfinalvalues;
				#echo "<pre/>" ;
				#print_r($row["actions"]) ;
			} else {
				$storeTemplate["actions"] = '';
			}
			#echo "<pre>";
			#print_r($row["actions"]);
			$storeTemplate["usage_limit"] = $data['usage_limit'];
			$storeTemplate["expiration_date"] = $data['expiration_date'];
			$storeTemplate["description"] = $data['description'];
			$storeTemplate["from_date"] = $data['from_date'];
			$storeTemplate["to_date"] = $data['to_date'];
			$storeTemplate["uses_per_customer"] = $data['uses_per_customer'];
	
			$finalvaluesfromoptions="";
			
			#$select_qry2 = "SELECT customer_group_id FROM ".$salesrule_customer_group." WHERE rule_id = '".$data['rule_id']."'";
			if($this->getEdition() == "Community") { 
				$select_qry2 = $connection->select()->from($salesrule_customer_group,['customer_group_id' => 'customer_group_id'])
													->where('rule_id=?', $data['rule_id']); //->where('row_id=?', $data['rule_id']);
			} else {
				$select_qry2 = $connection->select()->from($salesrule_customer_group,['customer_group_id' => 'customer_group_id'])
													->where('row_id=?', $data['rule_id']);
			}
			$rows2 = $resource->getconnection()->fetchAll($select_qry2);
			foreach($rows2 as $data2)
			{ 
				$finalvaluesfromoptions .= $data2['customer_group_id'] . ",";
			}
			$okcleanedfinalvalues = substr_replace($finalvaluesfromoptions,"",-1);
			$storeTemplate["customer_group_ids"] = $okcleanedfinalvalues;
			
			$storeTemplate["is_active"] = $data['is_active'];
			$storeTemplate["is_primary"] = $data['is_primary'];
			$storeTemplate["stop_rules_processing"] = $data['stop_rules_processing'];
			$storeTemplate["is_advanced"] = $data['is_advanced'];
			$storeTemplate["product_ids"] = $data['product_ids'];
			$storeTemplate["sort_order"] = $data['sort_order'];
			$storeTemplate["simple_action"] = $data['simple_action'];
			$storeTemplate["discount_amount"] = $data['discount_amount'];
			$storeTemplate["discount_qty"] = $data['discount_qty'];
			$storeTemplate["discount_step"] = $data['discount_step'];
			$storeTemplate["simple_free_shipping"] = $data['simple_free_shipping'];
			$storeTemplate["apply_to_shipping"] = $data['apply_to_shipping'];
			$storeTemplate["times_used"] = $data['times_used'];
			$storeTemplate["is_rss"] = $data['is_rss'];
			$storeTemplate["coupon_type"] = $data['coupon_type'];
			$storeTemplate["use_auto_generation"] = $data['use_auto_generation'];
			$storeTemplate["type"] = $data['type'];
			$storeTemplate["coupon_times_used"] = $data['coupon_times_used'];
			
			$finalvaluesfromoptions="";
			
			#$select_qry2 = "SELECT website_id FROM ".$salesrule_website." WHERE rule_id = '".$data['rule_id']."'";
			if($this->getEdition() == "Community") { 
				$select_qry2 = $connection->select()->from($salesrule_website,['website_id' => 'website_id'])
													->where('rule_id=?', $data['rule_id']);
			} else {
				$select_qry2 = $connection->select()->from($salesrule_website,['website_id' => 'website_id'])
													->where('row_id=?', $data['rule_id']);
			}
												
			$rows2 = $resource->getconnection()->fetchAll($select_qry2);
			foreach($rows2 as $data2)
			{ 
				$finalvaluesfromoptions .= $data2['website_id'] . ",";
			}
			$okcleanedwebsiteids = substr_replace($finalvaluesfromoptions,"",-1);
			$storeTemplate["website_ids"] = $okcleanedwebsiteids;
			
			$dataExport->addData($storeTemplate);
            $content .= $dataExport->toString($template) . "\n";

	   }	
		#exit;
        //$content .= $template . "\n";
        
        return $this->fileFactory->create('export_shopping_cart_rules.csv', $content, DirectoryList::VAR_DIR);
    }

	public function getEdition()
    {
		$magentoVersion = $this->_objectManager->create('Magento\Framework\App\ProductMetadataInterface');
        return $magentoVersion->getEdition();
    }
	public function getVersion()
    {
		$magentoVersion = $this->_objectManager->create('Magento\Framework\App\ProductMetadataInterface');
        return $magentoVersion->getVersion();
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_ShoppingCartRulesImportExport::import_export'
        );

    }
}

<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ShoppingCartRulesImportExport\Model\Data;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 *  CSV Import Handler
 */
 
class CsvImportHandler
{
     /**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
	
	/**
     * Write connection adapter
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;
    /**
     * Categoiry factory
     *
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        ResourceConnection $resource,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        // prevent admin store from loading
        $this->_resource = $resource;
        $this->_categoryFactory = $categoryFactory;
        $this->csvProcessor = $csvProcessor;
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('name'),
            1 => __('label'),
            2 => __('codecode'),
            3 => __('conditions_aggregator'),
            4 => __('conditions'),
            5 => __('actions_aggregator'),
            6 => __('actions'),
            7 => __('usage_limit'),
            8 => __('expiration_date'),
            9 => __('description'),
            10 => __('from_date'),
            11 => __('to_date'),
            12 => __('uses_per_customer'),
            13 => __('customer_group_ids'),
            14 => __('is_active'),
            15 => __('is_primary'),
            16 => __('stop_rules_processing'),
            17 => __('is_advanced'),
            18 => __('product_ids'),
            19 => __('sort_order'),
            20 => __('simple_action'),
            21 => __('discount_amount'),
            22 => __('discount_qty'),
            23 => __('discount_step'),
            24 => __('simple_free_shipping'),
            25 => __('apply_to_shipping'),
            26 => __('times_used'),
            27 => __('is_rss'),
            28 => __('coupon_type'),
            29 => __('use_auto_generation'),
            30 => __('type'),
            31 => __('coupon_times_used'),
            32 => __('website_ids')
        ];
    }

    /**
     * Import Categories from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file, $params)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
		
		if($params['import_delimiter'] != "") { $this->csvProcessor->setDelimiter($params['import_delimiter']); }
		if($params['import_enclose'] != "") { $this->csvProcessor->setEnclosure($params['import_enclose']); }
		
        $RawData = $this->csvProcessor->getData($file['tmp_name']);
        // first row of file represents headers
        $fileFields = $RawData[0];
		
        $ratesData = $this->_filterData($fileFields, $RawData);
		
        foreach ($ratesData as $dataRow) {
            $regionsCache = $this->_importRate($dataRow, $params);
        }
    }


    /**
     * Filter data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $RawDataHeader
     * @param array $RawData
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
     protected function _filterData(array $RawDataHeader, array $RawData)
    {
		$rowCount=0;
		$RawDataRows = array();
		
        foreach ($RawData as $rowIndex => $dataRow) {
			// skip headers
            if ($rowIndex == 0) {
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
			/* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
				foreach ($dataRow as $rowIndex => $dataRowNew) {
					$RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
				}
			}
			$rowCount++;
        }
        return $RawDataRows;
    }

	public function getEdition()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$magentoVersion = $objectManager->create('Magento\Framework\App\ProductMetadataInterface');
        return $magentoVersion->getEdition();
    }
	public function getVersion()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$magentoVersion = $objectManager->create('Magento\Framework\App\ProductMetadataInterface');
        return $magentoVersion->getVersion();
    }
    /**
     * Import single category
     *
     * @param array $Data
     * @param array $params 
     * @return array regions cache populated with regions related to country of imported tax rate
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _importRate(array $Data, array $params)
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		if($this->getVersion() > '2.2.0') {
			$jsonSerializer = $objectManager->get('Magento\Framework\Serialize\Serializer\Json');
		}
		$connection = $resource->getConnection();

		$salesrule = $resource->getTableName('salesrule') ; //gives table name with prefix
		$salesrule_coupon = $resource->getTableName('salesrule_coupon');
		$salesrule_website = $resource->getTableName('salesrule_website');
		$salesrule_customer_group = $resource->getTableName('salesrule_customer_group');
		$salesrule_label = $resource->getTableName('salesrule_label');
		$sequence_salesrule = $resource->getTableName('sequence_salesrule');
		$value = $Data;
	
		if($value['from_date'] != ""){
			$finalfromdate = $value['from_date'];
		} else {
			$finalfromdate = NULL;
		}
		if($value['to_date'] != "") {
			$finaltodate = $value['to_date'];
		} else {
			$finaltodate = NULL;
		}
		if($value['is_primary'] != "") {
			$is_primary = $value['is_primary'];
		} else {
			$is_primary = NULL;
		}

		$productIds = $value['product_ids'];

		if(isset($value['conditions_aggregator']) && $value['conditions_aggregator'] != ""){
			$final_conditions_aggregator = $value['conditions_aggregator'];
		} else {
			$final_conditions_aggregator = "all";
		}


		if(isset($value['actions_aggregator']) && $value['actions_aggregator'] != "") {
			$final_actions_aggregator = $value['actions_aggregator'];
		} else {
			$final_actions_aggregator = "any";
		}
					
		$conditions = array();
		$actions_conditions = array();
		$ctype = '';
		$cond = array();
		$act = array();
		
		if(isset($value['conditions'])) {
			if($value['conditions'] != "") {
				$conditions = array();
				$subconditions = array();
				$conditionsCount = 0;
				$skipNextRecord = false;
				//Magento\SalesRule\Model\Rule\Condition\Product\Subselect~qty~>=~10^Magento\SalesRule\Model\Rule\Condition\Product~discount_group~==~147
				$conditionsforimport = explode('^',$value['conditions']);
				foreach ($conditionsforimport as $conditionsvalue) {
					$conditionsvalues = explode('~',$conditionsvalue);
					if($skipNextRecord) { $$skipNextRecord = false; continue; }
					$conditions_type = isset($conditionsvalues[0]) ? $conditionsvalues[0] : 'Magento\\SalesRule\\Model\\Rule\\Condition\\Address';
					if($conditions_type == "Magento\SalesRule\Model\Rule\Condition\Product\Subselect") {
						
						$subconditionsvalues = explode('~',$conditionsforimport[$conditionsCount+1]);
						$subconditions[] = array(
						'type'=> isset($subconditionsvalues[0]) ? $subconditionsvalues[0] : '',//'salesrule/rule_condition_address'
						'attribute' => isset($subconditionsvalues[1]) ? $subconditionsvalues[1] : NULL,
						'operator' => isset($subconditionsvalues[2]) ? $subconditionsvalues[2] : NULL,
						'value' => isset($subconditionsvalues[3]) ? $subconditionsvalues[3] : '',
						'is_value_processed' => NULL
						);
						
						$conditions[] = array(
						'aggregator'=> $final_conditions_aggregator,
						'type'=> $conditions_type,//'salesrule/rule_condition_address'
						'attribute' => isset($conditionsvalues[1]) ? $conditionsvalues[1] : NULL,
						'conditions' => $subconditions,
						'operator' => isset($conditionsvalues[2]) ? $conditionsvalues[2] : NULL,
						'value' => isset($conditionsvalues[3]) ? $conditionsvalues[3] : '',
						'is_value_processed' => NULL
						);
						$skipNextRecord = true;
						
					} else {
						$conditions[] = array(
						'type'=> $conditions_type,//'salesrule/rule_condition_address'
						'attribute' => isset($conditionsvalues[1]) ? $conditionsvalues[1] : NULL,
						'operator' => isset($conditionsvalues[2]) ? $conditionsvalues[2] : NULL,
						'value' => isset($conditionsvalues[3]) ? $conditionsvalues[3] : '',
						'is_value_processed' => NULL
						);
					}
					$conditionsCount++;
				}
			} else {
				$conditions = array();
			}
		}

		//conditions_serialized
		$cond = array (
		'type'=>'Magento\\SalesRule\\Model\\Rule\\Condition\\Combine',//'salesrule/rule_condition_combine' //rule_condition_product_found
		'attribute'=>NULL,
		'operator'=>NULL,
		'value' => 1,
		'is_value_processed'=>'',
		'aggregator'=> $final_conditions_aggregator,
		'conditions'=> $conditions
		);
 

			//actions_serialized
		if(isset($value['actions'])) {
			if($value['actions'] != "") {
				$actions_conditions = array();
				$actionsforimport = explode('^',$value['actions']);
				foreach ($actionsforimport as $actionvalue) {
					
					$actionsvalues = explode('~',$actionvalue);
					$actions_conditions[] = array(
					'type'=> 'Magento\\SalesRule\\Model\\Rule\\Condition\\Product', //'salesrule/rule_condition_product'
					//'quote_item_qty' => '',
					'attribute' => $actionsvalues[0],
					'operator' => isset($actionsvalues[1]) ? $actionsvalues[1] : '',
					'value' => isset($actionsvalues[2]) ? $actionsvalues[2] : '',
					'is_value_processed' => ''
					);
				}
			} else {
				$actions_conditions = array();
			}
		}
		//actions_serialized
		$act = array (
			'type'=>'Magento\\SalesRule\\Model\\Rule\\Condition\\Product\\Combine', //'salesrule/rule_condition_product_combine'
			'attribute'=>'',
			'operator'=>'',
			'value' => 1,
			'is_value_processed'=>'',
			'aggregator'=> $final_actions_aggregator,
			'conditions'=> $actions_conditions
		);

		$insData = array(
		'name'=> $value['name'],
		'description'=> $value['description'],
		'from_date'=> $finalfromdate,
		'to_date'=> $finaltodate,
		'uses_per_customer'=> $value['uses_per_customer'],
		'is_active'=> $value['is_active'],
		'conditions_serialized'=> ($this->getVersion() > '2.2.0') ? $jsonSerializer->serialize($cond) : serialize($cond),
		'actions_serialized'=> ($this->getVersion() > '2.2.0') ? $jsonSerializer->serialize($act) : serialize($act),
		'stop_rules_processing'=> $value['stop_rules_processing'],
		'is_advanced'=> $value['is_advanced'],
		'product_ids'=> $productIds,
		'sort_order'=> $value['sort_order'],
		'simple_action'=> $value['simple_action'], //by_percent || cart_fixed
		'discount_amount'=> $value['discount_amount'],
		'discount_qty'=> ((is_numeric($value['discount_qty']) && $value['discount_qty'] > 0) ? $value['discount_qty'] : null),
		'discount_step'=> $value['discount_step'],
		'simple_free_shipping'=> $value['simple_free_shipping'], //0 1 or 2
		'apply_to_shipping'=> $value['apply_to_shipping'],
		'times_used'=> $value['times_used'],
		'is_rss'=> $value['is_rss'],
		'coupon_type'=> $value['coupon_type']
		);  		

		if(isset($Data['delete_rule'])) 
		{
			if ( $Data['delete_rule'] == 'Delete' || $Data['delete_rule'] == 'delete' ) {
				$select_qry = $resource->getConnection()->query("SELECT rule_id FROM ".$salesrule_coupon." WHERE code='".$Data['code']."';");
				$row = $select_qry->fetch();
				$rule_id = $row['rule_id'];
				$rs2 = $resource->getConnection()->query("DELETE FROM ".$salesrule." where rule_id='".$rule_id."';");
				// TODO: Implement execute() method.
				#$this->_redirect($this->_redirect->getRefererUrl()); 
                #$this->messageManager->addSuccess(__('Cart_Price_Rules_Deleted_SucessFully'));				
				return true;
			} 
		}	 
       
	    if($Data['coupon_type'] == "2") {
			//means we have a actual coupon code.. 
			/*$rs = $w->query("SELECT rule_id FROM ".$salesrule."salesrule_coupon WHERE code='".$Data['code']."';");*/
			$rs = $resource->getConnection()->query("SELECT ".$salesrule_coupon.".rule_id, ".$salesrule_coupon.".coupon_id FROM ".$salesrule_coupon." INNER JOIN ".$salesrule." ON ".$salesrule.".rule_id = ".$salesrule_coupon.".rule_id WHERE ".$salesrule.".name=\"".addslashes($Data['name'])."\" AND ".$salesrule_coupon.".code='".$Data['code']."';");
		} else {
			//else we match by name of rule
			$rs = $resource->getConnection()->query("SELECT rule_id FROM ".$salesrule." where name=\"".addslashes($Data['name'])."\";");
		}
		$rows = $rs->fetchAll();
		
		
		if (count($rows)){
			
			if($Data['label'] !="") {
			 	$labelsforimport = explode('^',$Data['label']);
				
				foreach ($labelsforimport as $labeldata){
					$individuallabels = explode('~',$labeldata);
						
					/*if($individuallabels[0] !="") { 
						$final_coupon_storeid = $individuallabels[0]; 
					} else { 
						$final_coupon_storeid = "1"; 
					}*/
					if(isset($individuallabels[1])) { 
						$final_coupon_label = $individuallabels[1]; 
					} else { 
						if($individuallabels[0] !="") {
							$final_coupon_label = $individuallabels[0];
						} else {
							$final_coupon_label = "";
						} 
					}
						
					$connection = $resource->getConnection()->update(''.$salesrule_label.'', array(
					'label'=> $final_coupon_label), 'rule_id=' . $rows[0]['rule_id'] .'');
				
				}
			}

			$connection = $resource->getConnection()->update(''.$salesrule.'', $insData, 'rule_id=' . $rows[0]['rule_id']);
			$connection = $resource->getConnection()->update(''.$salesrule_coupon.'',  array(
			'code'=>$Data['code'],
			'usage_limit'=>$Data['usage_limit'],
			'usage_per_customer'=>$Data['uses_per_customer'],
			'times_used'=>$Data['times_used'],
			'expiration_date'=>$Data['expiration_date'],
			'is_primary'=>$is_primary,
			), 'rule_id=' . $rows[0]['rule_id']);
				
				
			//adds coupon codes to website(s)	
			if($this->getEdition() == "Community") { 
				$rs2 = $resource->getConnection()->query("DELETE FROM ".$salesrule_website." WHERE rule_id='".$rows[0]['rule_id']."';");
			} else {
				$rs2 = $resource->getConnection()->query("DELETE FROM ".$salesrule_website." WHERE row_id='".$rows[0]['rule_id']."';");	
			}
			$websiteidsforimport = explode(',',$Data['website_ids']);
			foreach ($websiteidsforimport as $websiteidvalue) {
				
				if($this->getEdition() == "Community") { 
					$websiteData = array(
					'rule_id'=>$rows[0]['rule_id'],
					'website_id'=>$websiteidvalue
					);
				} else {
					$websiteData = array(
					'row_id'=>$rows[0]['rule_id'],
					'website_id'=>$websiteidvalue
					);
				}
				$connection = $resource->getConnection()->insert(''.$salesrule_website.'', $websiteData);
			}
			
			//adds customer group(s) to coupon		
			if($this->getEdition() == "Community") { 		
				$rs2 = $resource->getConnection()->query("DELETE FROM ".$salesrule_customer_group." WHERE rule_id='".$rows[0]['rule_id']."';");
			} else {
				$rs2 = $resource->getConnection()->query("DELETE FROM ".$salesrule_customer_group." WHERE row_id='".$rows[0]['rule_id']."';");
			}
			$customergroupidsforimport = explode(',',$Data['customer_group_ids']);
			foreach ($customergroupidsforimport as $customergroupid) {
				
				if($this->getEdition() == "Community") { 
					$customergroupData = array(
					'rule_id'=>$rows[0]['rule_id'],
					'customer_group_id'=>$customergroupid
					);
				} else {
					$customergroupData = array(
					'row_id'=>$rows[0]['rule_id'],
					'customer_group_id'=>$customergroupid
					);
				}
				$rs2 = $resource->getConnection()->insert(''.$salesrule_customer_group.'', $customergroupData);
			} 
			
		} else {
		
			if($this->getEdition() != "Community") { 
				$select_qry = $resource->getConnection()->query("SELECT sequence_value FROM ".$sequence_salesrule." ORDER BY sequence_value DESC;");
				$rowTemp = $select_qry->fetch();
				$shoppingcartruleid = $rowTemp['sequence_value']+1;
				$sequenceData['sequence_value'] = $shoppingcartruleid;
				$connection = $resource->getConnection()->insert(''.$sequence_salesrule.'', $sequenceData);
				$insData['row_id'] = $shoppingcartruleid;  	
				$insData['rule_id'] = $shoppingcartruleid;  
				$insData['updated_in'] = '2147483647';		
			}
			#print_r($insData);
			#exit;
			$connection = $resource->getConnection()->insert(''.$salesrule.'', $insData);
			
			if($this->getEdition() == "Community") { 
				$shoppingcartruleid = $resource->getConnection()->lastInsertId();
			}
			
			if($value['code'] !="") {
				$connection = $resource->getConnection()->insert(''.$salesrule_coupon.'', array(
				'rule_id'=>$shoppingcartruleid,
				'code'=>$value['code'],
				'usage_limit'=>$value['usage_limit'],
				'usage_per_customer'=>$value['uses_per_customer'],
				'times_used'=>$value['times_used'],
				'expiration_date'=>$value['expiration_date'],
				'is_primary'=>$value['is_primary']
				));
			}
				
			if($value['label'] !="") {
				$labelsforimport = explode('^',$value['label']);
				foreach ($labelsforimport as $labeldata) {
					$individuallabels = explode('~',$labeldata);
					
					if($individuallabels[0] !="") { 
						$final_coupon_storeid = $individuallabels[0];
					} else { 
						$final_coupon_storeid = "0";
					}
					
					if(isset($individuallabels[1])) { 
						$final_coupon_label = $individuallabels[1]; 
					} else { 
						if($individuallabels[0] !="") {
							$final_coupon_label = $individuallabels[0];
						} else {
							$final_coupon_label = "";
						} 
					}
					
					$connection1 = $resource->getConnection()->query("SELECT label_id FROM ".$salesrule_label." WHERE rule_id ='".$shoppingcartruleid."' AND store_id ='".$final_coupon_storeid."';");
					$row3 = $connection1->fetch();
					 
					if(empty($row3['label_id'])) {
							$connection = $resource->getConnection()->insert(''.$salesrule_label.'', array(
							'rule_id'=>$shoppingcartruleid,
							'store_id'=>$final_coupon_storeid,
							'label'=>$final_coupon_label
							));
					}
				}
			}
				
			$websiteidsforimport = explode(',',$value['website_ids']);
			foreach ($websiteidsforimport as $websiteidvalue) {
				
				if($this->getEdition() == "Community") { 
					$connection = $resource->getConnection()->insert(''.$salesrule_website.'', array(
					'rule_id'=>$shoppingcartruleid,
					'website_id'=>$websiteidvalue
					));

				} else {
					$connection = $resource->getConnection()->insert(''.$salesrule_website.'', array(
					'row_id'=>$shoppingcartruleid,
					'website_id'=>$websiteidvalue
					));
				}
			}
				
				
			if($value['customer_group_ids'] !=""){
				//adds customer group(s) to coupon			
				$customergroupidsforimport = explode(',',$value['customer_group_ids']);
				foreach ($customergroupidsforimport as $customergroupid) {
					
					if($this->getEdition() == "Community") { 
						$connection1 = $resource->getConnection()->query("SELECT rule_id FROM ".$salesrule_customer_group." WHERE rule_id ='".$shoppingcartruleid."' AND customer_group_id ='".$customergroupid."';");
						$row5 = $connection1->fetch();
						if(empty($row5['rule_id'])) {
							$connection = $resource->getConnection()->insert(''.$salesrule_customer_group.'', array(
							'rule_id'=>$shoppingcartruleid,
							'customer_group_id'=>$customergroupid
							));
						}
					} else {
						$connection1 = $resource->getConnection()->query("SELECT row_id FROM ".$salesrule_customer_group." WHERE row_id ='".$shoppingcartruleid."' AND customer_group_id ='".$customergroupid."';");
						$row5 = $connection1->fetch();
						if(empty($row5['row_id'])) {
							$connection = $resource->getConnection()->insert(''.$salesrule_customer_group.'', array(
							'row_id'=>$shoppingcartruleid,
							'customer_group_id'=>$customergroupid
							));
						}
					}
				}
			} 
        } 
    }
}
<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CustomerreviewsImportExport\Model\Data;

use Magento\Framework\App\ResourceConnection;

/**
 *  CSV Import Handler
 */
 
class CsvImportHandler
{
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
    /**
     * Collection of publicly available stores
     *
     * @var \Magento\Store\Model\Resource\Store\Collection
     */
    protected $_publicStores;

    /**
     * Reviews factory
     *
     * @var \Magento\Catalog\Model\ReviewsFactory
     */
    protected $_reviewsFactory;
	
    protected $_productFactory;
	
    protected $_ratingOptionFactory;

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @param \Magento\Store\Model\Resource\Store\Collection $storeCollection
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        ResourceConnection $resource,
        \Magento\Store\Model\ResourceModel\Store\Collection $storeCollection,
		\Magento\Review\Model\ReviewFactory $reviewsFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Review\Model\Rating\OptionFactory $ratingOptionFactory,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        // prevent admin store from loading
        $this->_resource = $resource;
        $this->_publicStores = $storeCollection->setLoadDefault(false);
        $this->_reviewsFactory = $reviewsFactory;
        $this->_productFactory = $productFactory;
        $this->_customerFactory = $customerFactory;
        $this->_ratingOptionFactory = $ratingOptionFactory;
        $this->csvProcessor = $csvProcessor;
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('created_at'),
            1 => __('review_title'),
            2 => __('review_detail'),
            3 => __('nickname'),
            4 => __('customer_id'),
            5 => __('product_id'),
            6 => __('entity_type'),
            7 => __('status_code'),
            8 => __('reviews_count'),
            9 => __('rating_summary'),
            10 => __('rating_options'),
            11 => __('store_ids')
        ];
    }

    /**
     * Import Data from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file, $params)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
		
		if($params['import_delimiter'] != "") { $this->csvProcessor->setDelimiter($params['import_delimiter']); }
		if($params['import_enclose'] != "") { $this->csvProcessor->setEnclosure($params['import_enclose']); }
		
        $RawData = $this->csvProcessor->getData($file['tmp_name']);
        // first row of file represents headers
        $fileFields = $RawData[0];
        $ratesData = $this->_filterData($fileFields, $RawData);
       
        foreach ($ratesData as $dataRow) {
            // skip headers
            #if ($rowIndex == 0) {
                #continue;
            #}
            $regionsCache = $this->_importReviews($dataRow, $params);
        }
		#exit;
    }

    /**
     * Filter data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $RawData
     * @param array $invalidFields assoc array of invalid file fields
     * @param array $validFields assoc array of valid file fields
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
     protected function _filterData(array $RawDataHeader, array $RawData)
    {
		$rowCount=0;
		$RawDataRows = array();
        foreach ($RawData as $rowIndex => $dataRow) {
			// skip headers
            if ($rowIndex == 0) {
				if(!in_array("created_at", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "created_at" NOT FOUND'));
				}
				if(!in_array("review_title", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "review_title" NOT FOUND'));
				}
				if(!in_array("entity_type", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "entity_type" NOT FOUND'));
				}
				if(!in_array("status_code", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "status_code" NOT FOUND'));
				}
				if(!in_array("reviews_count", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "reviews_count" NOT FOUND'));
				}
				if(!in_array("rating_summary", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "rating_summary" NOT FOUND'));
				}
				if(!in_array("rating_options", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "rating_options" NOT FOUND'));
				}
				if(!in_array("store_ids", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "store_ids" NOT FOUND'));
				}
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
			/* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
				foreach ($dataRow as $rowIndex => $dataRowNew) {
					$RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
				}
			}
			$rowCount++;
        }
        return $RawDataRows;
    }

    /**
     * Import Customer Reviews
     *
     * @param array $Data
     * @param array $storesCache cache of stores related to tax rate titles
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _importReviews(array $Data, array $params)
    {
		#print_r($Data);
	    $storearrayofids = array();
		$connection = $this->_resource->getConnection();
		$customerReviews = $this->_reviewsFactory->create();
	
		if(isset($Data['created_at'])) { $customerReviews->setCreatedAt($Data['created_at']); }
		if(isset($Data['review_title'])) { $customerReviews->setTitle($Data['review_title']); }
		if(isset($Data['review_detail'])) { $customerReviews->setDetail($Data['review_detail']); }
		if(isset($Data['nickname'])) { $customerReviews->setNickname($Data['nickname']); }
		if(isset($Data['customer_id'])) { 
			if(is_numeric($Data['customer_id'])) {
					$customer = $this->_customerFactory->create()->load($Data['customer_id']);
					$customerReviews->setCustomerId($customer->getId());
			} else {
				foreach ($this->_publicStores as $store) { $websiteid = $store->getWebsiteId(); }
				if($websiteid < 1) { $websiteid = 1; }
				if(isset($Data['email'])) {
					$customer = $this->_customerFactory->create()->setWebsiteId($websiteid)->loadByEmail($Data['email']);
					$customerReviews->setCustomerId($customer->getId()); 
				}
			}
		}
		
		if(isset($Data['product_id'])) { 
			if(is_numeric($Data['product_id'])) {
				$entityPkValue = $Data['product_id'];
				$customerReviews->setEntityPkValue($Data['product_id']); 
			}else {
					throw new \Magento\Framework\Exception\LocalizedException(__('ERROR CHECK "product_id" Column. Should only contain numbers.'));
			}
		}
		if(isset($Data['sku'])) { 
			if($Data['sku'] !="") { 
				$products = $this->_productFactory->create()->loadByAttribute('sku', $Data['sku']);
				if (!empty($products)) {
					$entityPkValue = $products->getId();
					$customerReviews->setEntityPkValue($products->getId()); 
				} else {
					throw new \Magento\Framework\Exception\LocalizedException(__('ERROR NO SKU ['.$Data['sku'].'] FOUND CANNOT IMPORT REVIEW'));
				}
			}
		}
		
		
		//\Magento\Review\Model\Review::ENTITY_PRODUCT_CODE
		if(isset($Data['entity_type'])) { $customerReviews->setEntityId($customerReviews->getEntityIdByCode($Data['entity_type'])); }
		if(isset($Data['status_code'])) { 
			if($Data['status_code'] == "Approved") {
				$customerReviews->setStatusId(\Magento\Review\Model\Review::STATUS_APPROVED); 
			} else if($Data['status_code'] == "Pending") {
				$customerReviews->setStatusId(\Magento\Review\Model\Review::STATUS_PENDING); 
			} else if($Data['status_code'] == "Not Approved") {
				$customerReviews->setStatusId(\Magento\Review\Model\Review::STATUS_NOT_APPROVED); 
			}
		}
		$customerReviews->setStoreId(1);
		if(isset($Data['reviews_count'])) { $customerReviews->setReviewsCount($Data['reviews_count']); } //
		if(isset($Data['rating_summary'])) { $customerReviews->setRatingSummary($Data['rating_summary']); } //
		if(isset($Data['store_ids'])) { 
			$delimiteddata = explode(',',$Data['store_ids']);
			foreach ($delimiteddata as $individualstoreID) {
				$storearrayofids[] = $individualstoreID;
			}
			$customerReviews->setStores($storearrayofids); 
		} 
		
		try {
			$customerReviews->save();
			//have to resave to set a date
			if(isset($Data['created_at'])) { 
				if($Data['created_at'] !="") { 
					$customerReviews->setCreatedAt($Data['created_at']); 
					$customerReviews->save();
				}
			}
			
		} catch (\Exception $e) {
			throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
		}
		
		if(isset($Data['rating_options'])) {
			$delimiteddataoptions = explode(',',$Data['rating_options']);
			$_rating_option = $this->_resource->getTableName('rating_option');
			foreach ($delimiteddataoptions as $ratingoptionvalue) {
				if($ratingoptionvalue !="") {
				   $ratingoptionarr = explode(':',$ratingoptionvalue);	
				   $select_qry = "SELECT option_id FROM ".$_rating_option." WHERE rating_id = '". $ratingoptionarr[0]. "' and value = '" . $ratingoptionarr[1] . "'";
				   $rowsRatingOption = $connection->fetchAll($select_qry);
				   foreach($rowsRatingOption as $_rowData)
				   { 
						$this->_ratingOptionFactory->create()->setOptionId(
							$_rowData['option_id']
						)->setRatingId(
							$ratingoptionarr[0]
						)->setReviewId(
							$customerReviews->getReviewId()
						)->setEntityPkValue(
							$entityPkValue
						)->addVote();
				   }
				}
			}
		}
		#print_r($customerReviews->getData());
		$customerReviews->aggregate();
		#exit;
    }
}
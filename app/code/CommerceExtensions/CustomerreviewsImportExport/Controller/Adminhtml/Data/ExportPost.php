<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CustomerreviewsImportExport\Controller\Adminhtml\Data;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportPost extends \CommerceExtensions\CustomerreviewsImportExport\Controller\Adminhtml\Data
{
    protected $productRepository;
	protected $customerModel;
	protected $reviewCollection;
	protected $fileFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Review\Model\ResourceModel\Review\Collection $reviewCollection
    ) {
        $this->fileFactory = $fileFactory;
        $this->_ProductRepository = $productRepository;
        $this->_CustomerModel = $customerModel;
        $this->_ReviewCollection = $reviewCollection;
        parent::__construct($context,$fileFactory);
    }
	
    /**
     * Export action from import/export customer reviews
     *
     * @return ResponseInterface
     */
	
    public function execute()
    {
		$params = $this->getRequest()->getParams();
		$_resource = $this->_objectManager->create('Magento\Framework\App\ResourceConnection');
		$connection = $_resource->getConnection();
		$_rating = $_resource->getTableName('rating');
		$_review_store = $_resource->getTableName('review_store');
		$_rating_option_vote = $_resource->getTableName('rating_option_vote');
			
		$ByStoreID = 0;
		
		if($params['export_delimiter'] != "") {
			$delimiter = $params['export_delimiter'];
		} else {
			$delimiter = ",";
		}
		if($params['export_enclose'] != "") {
			$enclose = $params['export_enclose'];
		} else {
			$enclose = "\"";
		}
		
        /** start csv content and set template */
        $reviewsContent = new \Magento\Framework\DataObject();
		
        $headers = new \Magento\Framework\DataObject(
            [
                'created_at' => __('created_at'),
                'review_title' => __('review_title'),
                'review_detail' => __('review_detail'),
                'nickname' => __('nickname'),
                'customer_id' => __('customer_id'),
                'product_id' => __('product_id'),
                'sku' => __('sku'),
                'entity_type' => __('entity_type'),
                'status_code' => __('status_code'),
                'reviews_count' => __('reviews_count'),
                'rating_summary' => __('rating_summary'),
                'rating_options' => __('rating_options'),
                'store_ids' => __('store_ids')
            ]
        );
        $template = ''.$enclose.'{{created_at}}'.$enclose.''.$delimiter.''.$enclose.'{{review_title}}'.$enclose.''.$delimiter.''.$enclose.'{{review_detail}}'.$enclose.''.$delimiter.''.$enclose.'{{nickname}}'.$enclose.''.$delimiter.''.$enclose.'{{customer_id}}'.$enclose.''.$delimiter.''.$enclose.'{{product_id}}'.$enclose.''.$delimiter.''.$enclose.'{{sku}}'.$enclose.''.$delimiter.''.$enclose.'{{entity_type}}'.$enclose.''.$delimiter.''.$enclose.'{{status_code}}'.$enclose.''.$delimiter.''.$enclose.'{{reviews_count}}'.$enclose.''.$delimiter.''.$enclose.'{{rating_summary}}'.$enclose.''.$delimiter.''.$enclose.'{{rating_options}}'.$enclose.''.$delimiter.''.$enclose.'{{store_ids}}'.$enclose.'';
		
        $content = $headers->toString($template);
        $storeTemplate = [];
        $content .= "\n";
		
        $customerreviews = $this->_ReviewCollection->load();
		
		//join the customer reviews review type (product/category/customer) table
		$customerreviews->getSelect()->join(
            ['review_entity' => $customerreviews->getTable('review_entity')],
            'main_table.entity_id = review_entity.entity_id',
            ['entity_code']
        );
		//join the customer reviews status table (approved/not approved/pending)
		$customerreviews->getSelect()->join(
            ['review_status' => $customerreviews->getTable('review_status')],
            'main_table.status_id = review_status.status_id',
            ['status_code']
        );
		
		//join the customer reviews summary table
		$customerreviews->getSelect()->joinLeft(
            ['review_entity_summary' => $customerreviews->getTable('review_entity_summary')],
            'main_table.review_id = review_entity_summary.primary_id',
            ['reviews_count','rating_summary']
        );
		$exportCustomerReviews = $customerreviews->getData();
		foreach ($exportCustomerReviews as $review) {
			
			$storeTemplate['created_at'] = $review['created_at'];
			$storeTemplate['review_title'] = $review['title'];
			
			
			if($enclose=="\"") {
				$storeTemplate['review_detail'] = str_replace('"',"''",$review['detail']);
			} else {
				$storeTemplate['review_detail'] = $review['detail'];
			}
			$storeTemplate['nickname'] = $review['nickname'];
			
			if($params['customers_by_email'] == "true") {
				if($review['customer_id'] != "") {
					$customer = $this->_CustomerModel->load($review['customer_id']);
					$storeTemplate['customer_id'] = $customer->getEmail();
				} else {
					$storeTemplate['customer_id'] = "";
				}
			} else {
				$storeTemplate['customer_id'] = $review['customer_id'];
			}
			if($params['reviews_by_sku'] == "true") {
				if($review['entity_pk_value'] != "") {
					try {
						$products = $this->_ProductRepository->getById($review['entity_pk_value']);
					} catch (\Exception $e) {
						$products = null;
					}
					if (!empty($products)) {
						$storeTemplate['sku'] = $products->getSku();
					} else {
						$storeTemplate['sku'] = "";
					}
				} else {
					$storeTemplate['sku'] = "";
				}
			} else {
				if($review['entity_pk_value'] != "") { $storeTemplate['product_id'] = $review['entity_pk_value']; } else { $storeTemplate['product_id'] = ""; }
			}
			$storeTemplate['entity_type'] = $review['entity_code'];
			$storeTemplate['status_code'] = $review['status_code'];
			$storeTemplate['reviews_count'] = $review['reviews_count'];
			$storeTemplate['rating_summary'] = $review['rating_summary'];
			
			//RATING OPTION DATA START
			$_ratingoptions = "";
			$_finalratingoptions = "";
			
			$query = "SELECT rating.rating_id, rating_option_vote.value FROM ".$_rating." As rating INNER JOIN ".$_rating_option_vote." AS rating_option_vote ON rating_option_vote.rating_id = rating.rating_id WHERE review_id = '".$review['review_id']."'";
			
			$searchtermsCollection = $connection->fetchAll($query);
			foreach ($searchtermsCollection as $row) {
				$_ratingoptions .= $row['rating_id'] . ":" . $row['value'] . ",";
			}
			$_finalratingoptions = substr_replace($_ratingoptions,"",-1);
			$storeTemplate['rating_options'] = $_finalratingoptions;
			//RATING OPTION DATA END
			
			//REVIEW STORE IDS START
			$_finalstoreidsforexport="";
			if($ByStoreID != "") {
				$select__store_ids_qry = "SELECT store_id FROM ".$_review_store." WHERE review_id = '".$review['review_id']."' AND store_id = '".$ByStoreID."'";
			} else {
				$select__store_ids_qry = "SELECT store_id FROM ".$_review_store." WHERE review_id = '".$review['review_id']."'";
			}
			$storeidrows = $connection->fetchAll($select__store_ids_qry);
			foreach($storeidrows as $datastoreid)
			{ 
			 $_finalstoreidsforexport .= $datastoreid['store_id'] . ",";
			}
			$_finalstoreidsforexport = substr_replace($_finalstoreidsforexport,"",-1);
			$storeTemplate['store_ids'] = $_finalstoreidsforexport;
			//REVIEW STORE IDS END
			
			$reviewsContent->addData($storeTemplate);
            $content .= $reviewsContent->toString($template) . "\n";
			#exit;
			
		}
		#print_r($content);
		#exit;
        //$content .= $template . "\n";
        
        return $this->fileFactory->create('export_customer_reviews.csv', $content, DirectoryList::VAR_DIR);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_CustomerreviewsImportExport::import_export'
        );

    }
}

<?php

/**
 * Copyright © 2016 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\AdvancedcustomerImportExport\Model\Data;

use Magento\Framework\App\ResourceConnection;

/**
 *  CSV Import Handler
 */
 
class CsvImportHandler
{ 
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;

    /**
     * Customer factory
     *
     * @var \Magento\Catalog\Model\CustomerFactory
     */
    protected $_customerFactory;
	
	protected $_regions;
	
    protected $_customerGroups;
    /**
     * @var \Magento\Store\Model\StoreManagerInterfaceFactory
     */
    protected $storeManagerInterfaceFactory;
    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

	/* REMOVE ATTRIBUTES WE DO NOT WANT TO EXPORT */ 
    protected $_disabledAttributes = ['default_billing', 'default_shipping', 'increment_id', 'entity_id', 'entity_id', 'website_id', 'group_id'];
    protected $_disabledAddressAttributes = ['entity_type_id', 'increment_id', 'parent_id', 'attribute_set_id', 'entity_id', 'region_id'];
	
	const PASSWORD_HASH = 0;
  	const PASSWORD_SALT = 1;
    /**
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory,
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
		\Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Store\Model\StoreManagerInterfaceFactory $storeManagerInterfaceFactory,
		\Magento\Customer\Model\ResourceModel\Group\CollectionFactory $customerGroupCollectionFactory,
		\Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
		\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        // prevent admin store from loading
        $this->_resource = $resource;
        $this->scopeConfig = $scopeConfig;
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->storeManagerInterfaceFactory     = $storeManagerInterfaceFactory;
        $this->_customerGroupCollectionFactory = $customerGroupCollectionFactory;
		$this->_regionFactory = $regionFactory;
        $this->_regionCollectionFactory = $regionCollectionFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->_customerModel = $customerFactory->create();
        $this->_addressModel = $addressFactory->create();
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
		/* BUILD OUT COLUMNS IN DEFAULT ATTRIBUTES */
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customer_attributes = $objectManager->get('Magento\Customer\Model\Customer')->getAttributes();
		$customer_address_attributes = $objectManager->get('Magento\Customer\Model\Address')->getAttributes();
		foreach($customer_attributes as $cal=>$val){
			if (!in_array($cal, $this->_disabledAttributes)) {
				$attributesArray[] = $cal;
			}
		}
        foreach($customer_address_attributes as $address_cal=>$address_val){
			if (!in_array($address_cal, $this->_disabledAddressAttributes)) {
				/* PREFIX ADDRESS FOR BILLING */
				$attributesArray[] = "billing_".$address_cal;
				/* PREFIX ADDRESS FOR SHIPPING */
				$attributesArray[] = "shipping_".$address_cal;
			} 
		}
		return $attributesArray;
    }

    /**
     * Import Data from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file, $params)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
		
		if($params['import_delimiter'] != "") { $this->csvProcessor->setDelimiter($params['import_delimiter']); }
		if($params['import_enclose'] != "") { $this->csvProcessor->setEnclosure($params['import_enclose']); }
		
        $RawData = $this->csvProcessor->getData($file['tmp_name']);
        // first row of file represents headers
        $fileFields = $RawData[0];
        $customerData = $this->_filterData($fileFields, $RawData);
		$rowCounter=1;
		
        foreach ($customerData as $dataRow) {
			try {
            	$this->_importCustomers($dataRow, $params);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
        		if ($this->_getStoreConfig('allowdebuglogs/general/enabled', 0)){
					$cronLogErrors[] = array("import_customers", "ROW: " . $rowCounter, "ERROR: " . $e->getMessage());
					$this->writeToCsv($cronLogErrors);	
				} else {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ROW: ' . $rowCounter . ' ERROR: ' . $e->getMessage()), $e);
				}
            }
            catch (\Exception $e){
        		if ($this->_getStoreConfig('allowdebuglogs/general/enabled', 0)){
					$cronLogErrors[] = array("import_customers", "ROW: " . $rowCounter, "ERROR: " . $e->getMessage());
					$this->writeToCsv($cronLogErrors);	
				} else {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ROW: ' . $rowCounter . ' ERROR: ' . $e->getMessage()), $e);
				}
            }
			$rowCounter++;
        }
    }

    protected function _getStoreConfig($path, $storeId)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }
	
	protected function writeToCsv($data) {
		#$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		#$directoryList = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
		#$csvProcessor = $objectManager->get('Magento\Framework\File\Csv');
		#$fileDirectoryPath = $directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
		$fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
	
		if(!is_dir($fileDirectoryPath))
			mkdir($fileDirectoryPath, 0777, true);
		$fileName = 'ce_customer_import_log_errors.csv';
		$filePath =  $fileDirectoryPath . '/' . $fileName;
	
		#$data2 = [];
		/* pass data array to write in csv file */
		#$data2 = [['column 1','column 2','column 3'],['100001','test','test2']];
		
		#$csvProcessor
		$this->csvProcessor
			->setEnclosure('"')
			->setDelimiter(',')
			->saveData($filePath, $data);
	
		return true;
	}

    /**
     * Filter data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $RawDataHeader
     * @param array $RawData
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _filterData(array $RawDataHeader, array $RawData)
    {
		$rowCount=0;
		$RawDataRows = array();
        foreach ($RawData as $rowIndex => $dataRow) {
			// skip headers
            if ($rowIndex == 0) {
				if(!in_array("email", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "email" NOT FOUND'));
				}
				if(!in_array("group", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "group" NOT FOUND'));
				}
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
			/* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
				foreach ($dataRow as $rowIndex => $dataRowNew) {
					$RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
				}
			}
			$rowCount++;
        }
        return $RawDataRows;
    }

	
    public function getRegionId($country, $regionName)
    {
        if (is_null($this->_regions)) {
            $this->_regions = array();
            $regionsCollection = $this->_regionCollectionFactory->create()->load();
            foreach ($regionsCollection as $region) {
                //$countryRegions[$region->getCountryId()][$region->getId()] = $region->getDefaultName();
                if (!isset($this->_regions[$region->getCountryId()])) {
                    $this->_regions[$region->getCountryId()] = array();
                }
                $this->_regions[$region->getCountryId()][$region->getDefaultName()] = $region->getId();
            }
        }

        if (isset($this->_regions[$country][$regionName])) {
            return $this->_regions[$country][$regionName];
        }

        return 0;
    }
	/**
     * Retrieve customer group collection array
     *
     * @return array
     */
    public function getCustomerGroups($newGroupAdded)
    {
        if (is_null($this->_customerGroups) || $newGroupAdded) {
            $this->_customerGroups = array();
			$collection = $this->_customerGroupCollectionFactory->create()->addFieldToFilter('customer_group_id', array('gt'=> 0))->load();
            foreach ($collection as $group) {
                $this->_customerGroups[$group->getCustomerGroupCode()] = $group->getId();
            }
        }
        return $this->_customerGroups;
    }

	public function upgradeCustomerHash($recordAttributesData)
	{
			if (isset($recordAttributesData['password_hash'])) {
				$hash = $this->explodePasswordHash($recordAttributesData['password_hash']);
	
				if (strlen($hash[self::PASSWORD_HASH]) == 32) {
					$recordAttributesData['password_hash'] = implode(
						':',
						[$hash[self::PASSWORD_HASH], $hash[self::PASSWORD_SALT], '0']
					);
				} elseif (strlen($hash[self::PASSWORD_HASH]) == 64) {
					$recordAttributesData['password_hash'] = implode(
						':',
						[$hash[self::PASSWORD_HASH], $hash[self::PASSWORD_SALT], '1']
					);
				}
			}
			return $recordAttributesData['password_hash'];
	}

    /**
     * @param string $passwordHash
     * @return array
     */
    public function explodePasswordHash($passwordHash)
    {
        $explodedPassword = explode(':', $passwordHash, 2);
        $explodedPassword[self::PASSWORD_SALT] = isset($explodedPassword[self::PASSWORD_SALT]) ? $explodedPassword[self::PASSWORD_SALT] : '';
        return $explodedPassword;
    }
    /**
     * Import Customers
     *
     * @param array $Data
     * @param array $params
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _importCustomers(array $Data, array $params)
    {
		#print_r($Data);
		$newGroupAdded = false;
		$connection    = $this->_resource->getConnection();
        #$website = $this->getWebsiteByCode($importData['website']);
		$customerModel = $this->_customerFactory->create();
        $storeManager  = $this->storeManagerInterfaceFactory->create();
        $websiteId     = $storeManager->getWebsite($Data['website'])->getId();
		$customer      = $customerModel->setWebsiteId($websiteId)->loadByEmail($Data['email']);
		
		
		if ($params['use_customer_billing_shipping_names'] == "true") {
			if(empty($Data['firstname'])) {
				if(!empty($Data['billing_firstname'])) {
					$Data['firstname'] = $Data['billing_firstname'];
				} elseif(!empty($Data['shipping_firstname'])) {
					$Data['firstname'] = $Data['shipping_firstname'];
				}
			}
			if(empty($Data['lastname'])) {
				if(!empty($Data['billing_lastname'])) {
					$Data['lastname'] = $Data['billing_lastname'];
				} elseif(!empty($Data['shipping_firstname'])) {
					$Data['lastname'] = $Data['shipping_lastname'];
				}
			}
			if(empty($Data['billing_firstname']) && !empty($Data['billing_city'])) {
				if(!empty($Data['firstname'])) {
					$Data['billing_firstname'] = $Data['firstname'];
				}
			}
			if(empty($Data['shipping_firstname']) && !empty($Data['shipping_city'])) {
				if(!empty($Data['firstname'])) {
					$Data['shipping_firstname'] = $Data['firstname'];
				}
			}
			if(empty($Data['billing_lastname']) && !empty($Data['billing_city'])) {
				if(!empty($Data['lastname'])) {
					$Data['billing_lastname'] = $Data['lastname'];
				}
			}
			if(empty($Data['shipping_lastname']) && !empty($Data['shipping_city'])) {
				if(!empty($Data['lastname'])) {
					$Data['shipping_lastname'] = $Data['lastname'];
				}
			}
		}
		
        if (!$customer->getId()) {
			
            // create new customer
			if ($params['insert_customer_id'] == "true") {
				if (!empty($Data['customer_id'])) {
					$query = "ALTER TABLE ".$this->_resource->getTableName('customer_entity')." AUTO_INCREMENT = ".$Data['customer_id']."";
					$new_customer_id = $connection->query($query);
				}
			}
			//insert customer group if doesn't exist
			if($params['auto_create_customer_groups'] == "true") { 
				$code = isset($Data['group']) ? $Data['group'] : '';
				$collection = $this->_customerGroupCollectionFactory->create()->addFieldToFilter('customer_group_code', $code)->load();// filter by group code
				if(!empty($code)) {
		 			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$group = $objectManager->create('Magento\Customer\Model\Group')->load($collection->getFirstItem()->getId()); 
					$group->setCode($code); //set the code
					$group->setTaxClassId(3); //set tax class. 3 is default value
					$group->save();
					$newGroupAdded = true;
				}
			}
            /**
             * Check customer group
             */
			$customerGroups = $this->getCustomerGroups($newGroupAdded);
            if (isset($customerGroups[$Data['group']])) {
                $customerModel->setGroupId($customerGroups[$Data['group']]);
            }
			
				
			/* Lets Import Customer Data */
			$customer_attributes = $customerModel->getAttributes();
			foreach($customer_attributes as $cal=>$val){
				if (!in_array($cal, $this->_disabledAttributes)) {
					if(isset($Data[$cal])) { $customerModel->setData($cal, $Data[$cal]); }
				}
			}
			//set website for  new customer
            $customerModel->setWebsiteId($websiteId);
			
			// password change/set
			if (isset($Data['password']) && strlen($Data['password'])) {
				$customer_password_hash = $customerModel->hashPassword($Data['password']);
				$customerModel->setData('password_hash', $customer_password_hash);
			}
			
			try {
				$customer = $customerModel->save();
			} catch (\Exception $e) {
				throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
			}
			/* Lets Import Addresses AFTER */
			$billingAttributesArray = array();
			$shippingAttributesArray = array();
			$customer_attributes = $this->_customerModel->getAttributes();
			$customer_address_attributes = $this->_addressModel->getAttributes();
			$containsBillingAddress = false;
			$containsShippingAddress = false;
			
			foreach($customer_address_attributes as $address_cal=>$address_val){
				if (!in_array($address_cal, $this->_disabledAddressAttributes)) {
					/* PREFIX ADDRESS FOR BILLING */
					if(!empty($Data["billing_".$address_cal])) { 
						$billingAttributesArray[$address_cal] = $Data["billing_".$address_cal]; 
						$containsBillingAddress = true;	
					}
					/* PREFIX ADDRESS FOR SHIPPING */
					if(!empty($Data["shipping_".$address_cal])) { 
						$shippingAttributesArray[$address_cal] = $Data["shipping_".$address_cal]; 
						$containsShippingAddress = true;
					}
				} 
			}
			
			if($containsBillingAddress || $containsShippingAddress) {	
				/* set billing address parent_id and region_id */
				if(!empty($billingAttributesArray)) {
					$regionBillingName = isset($Data['billing_region']) ? $Data['billing_region'] : '';
					if(isset($Data['billing_country_id'])) { 
						$billing_country_id = $Data['billing_country_id']; 
					} else if(isset($Data['billing_country'])) {  
						$billing_country_id = $Data['billing_country'];
					} else {
						throw new \Magento\Framework\Exception\LocalizedException(
							__('ERROR: CSV MUST HAS A COLUMN EITHER billing_country or billing_country_id')
						);
					}
					$billingRegionId = $this->getRegionId($billing_country_id, $regionBillingName);
					$billingAttributesArray['region_id'] = $billingRegionId;
					$billingAttributesArray['parent_id'] = $customer->getId();
				}
				if(!empty($shippingAttributesArray)) {
					/* set shipping address parent_id and region_id */
					$regionShippingName = isset($Data['shipping_region']) ? $Data['shipping_region'] : '';
					if(isset($Data['shipping_country_id'])) { 
						$shipping_country_id = $Data['shipping_country_id']; 
					} else if(isset($Data['shipping_country'])) {  
						$shipping_country_id = $Data['shipping_country'];
					} else {
						throw new \Magento\Framework\Exception\LocalizedException(
							__('ERROR: CSV MUST HAS A COLUMN EITHER shipping_country or shipping_country_id')
						);
					}
					$shippingRegionId = $this->getRegionId($shipping_country_id, $regionShippingName);
					$shippingAttributesArray['region_id'] = $shippingRegionId;
					$shippingAttributesArray['parent_id'] = $customer->getId();
				}
				
				//SET ADDRESS AS DEFAULT BILLING
				if(isset($Data["is_default_billing"]) && !empty($billingAttributesArray)) { 
					if($Data["is_default_billing"] == 1 && $Data["is_default_shipping"] == 1 ) {
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						$customer->setData('default_billing', $billingaddressInserted->getId());
						$customer->setData('default_shipping', $billingaddressInserted->getId());
					} else if($Data["is_default_billing"] == 1 && $Data["is_default_shipping"] == 0) { 
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						$customer->setData('default_billing', $billingaddressInserted->getId());
					} else if($Data["is_default_billing"] == 0 && $Data["is_default_shipping"] == 0) { 
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						$customer->setData('default_billing', 0);
						$customer->setData('default_shipping', 0);
					}
				} else {
					$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
					$billingaddressInserted->save();
					$customer->setData('default_billing', $billingaddressInserted->getId());
					$customer->setData('default_shipping', $billingaddressInserted->getId());
				}
				
				//SET ADDRESS AS DEFAULT SHIPPING
				if(isset($Data["is_default_shipping"]) && !empty($shippingAttributesArray)) { 
					if($Data["is_default_billing"] == 0 && $Data["is_default_shipping"] == 1) { 
						$shippingaddressInserted = $this->_addressModel->setData($shippingAttributesArray);
						$shippingaddressInserted->save(); 
						$customer->setData('default_shipping', $shippingaddressInserted->getId());
					} 
				}
			
			} //end check on if we row contains a billing or shipping address
			
			/* START FORCE OLD CUSTOMER DATE */
			if(isset($Data['customer_since']) && !isset($Data['created_at'])) {
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$importDate = date("Y-m-d H:i:s", $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime')->timestamp($Data['customer_since']));
				$customer->setCreatedAt($importDate);
			}
			/* END FORCE OLD CUSTOMER DATE */
			
			if ($params['convert_m1_hash_to_m2_hash'] == "true") {
				if($customerModel->getPasswordHash() !="") {
					//add a check for last 2 char in string == :0 or :1
					$checkPasswordHash = substr($customerModel->getPasswordHash(), -2);
					if($checkPasswordHash != ":0" || $checkPasswordHash != ":1") {
						$someData['password_hash'] = $customerModel->getPasswordHash();
						$customer_password_hash = $this->upgradeCustomerHash($someData);
						$customerModel->setData('password_hash', $customer_password_hash);
					}
				}
			}
			#exit;
			try {
				$customer->save();
			} catch (\Exception $e) {
				throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
			}
			
			if (isset($Data['is_subscribed'])) {
				#$customerModel->setData('is_subscribed', $Data['is_subscribed']);
				# get just generated subscriber
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$subscriber = $objectManager->create('Magento\Newsletter\Model\Subscriber')->loadByCustomerId($customer->getId());
				$subscriber->setStoreId($customer->getStoreId());
				$subscriber->setCustomerId($customer->getId());
				$subscriber->setSubscriberEmail($Data['email']);
				# change status to “subscribed” and save
				if($Data['is_subscribed'] == "1") {
					$subscriber->setSubscriberStatus(\Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED);
				} else {
					$subscriber->setSubscriberStatus(\Magento\Newsletter\Model\Subscriber::STATUS_UNSUBSCRIBED);
				}
				$subscriber->save();
			}
			/*
			if($params['email_customer_password'] == "true") { 
				$customer->sendNewAccountEmail();
			} 
			*/
        } else {
			
			if($params['update_customer_password'] != "true") { 
				// password update
				if (isset($Data['password'])) {
					unset($Data['password']);
				}
				if (isset($Data['password_hash'])) {
					unset($Data['password_hash']);
				}
			}
			if($params['auto_create_customer_groups'] == "true") { 
				$code = isset($Data['group']) ? $Data['group'] : '';
				$collection = $this->_customerGroupCollectionFactory->create()->addFieldToFilter('customer_group_code', $code)->load();// filter by group code
				if(!empty($code)) {
		 			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$group = $objectManager->create('Magento\Customer\Model\Group')->load($collection->getFirstItem()->getId()); 
					$group->setCode($code); //set the code
					$group->setTaxClassId(3); //set tax class. 3 is default value
					$group->save();
					$newGroupAdded = true;
				}
			}
			$customer_attributes = $this->_customerModel->getAttributes();
			#$customerModel = $this->_customerModel->load($customer->getId());
            /**
             * Check customer group
             */
			$customerGroups = $this->getCustomerGroups($newGroupAdded);
            if (isset($customerGroups[$Data['group']])) {
                $customer->setGroupId($customerGroups[$Data['group']]);
            }
			/* Lets Import Addresses First */
			$billingAttributesArray = array();
			$shippingAttributesArray = array();
			$customer_address_attributes = $this->_addressModel->getAttributes();
			$containsBillingAddress = false;	
			$containsShippingAddress = false;
			
			foreach($customer_address_attributes as $address_cal=>$address_val){
				if (!in_array($address_cal, $this->_disabledAddressAttributes)) {
					/* PREFIX ADDRESS FOR BILLING */
					if(!empty($Data["billing_".$address_cal])) { 
						$billingAttributesArray[$address_cal] = $Data["billing_".$address_cal]; 
						$containsBillingAddress = true;	
					}
					/* PREFIX ADDRESS FOR SHIPPING */
					if(!empty($Data["shipping_".$address_cal])) { 
						$shippingAttributesArray[$address_cal] = $Data["shipping_".$address_cal]; 
						$containsShippingAddress = true;
					}
				} 
			}
			
			
			if($containsBillingAddress || $containsShippingAddress) {	
				/* set billing address parent_id and region_id */
				if(!empty($billingAttributesArray)) {
					$regionBillingName = isset($Data['billing_region']) ? $Data['billing_region'] : '';
					if(isset($Data['billing_country_id'])) { 
						$billing_country_id = $Data['billing_country_id']; 
					} else if(isset($Data['billing_country'])) {  
						$billing_country_id = $Data['billing_country'];
					} else {
						throw new \Magento\Framework\Exception\LocalizedException(
							__('ERROR: CSV MUST HAS A COLUMN EITHER billing_country or billing_country_id')
						);
					}
					$billingRegionId = $this->getRegionId($billing_country_id, $regionBillingName);
					$billingAttributesArray['region_id'] = $billingRegionId;
					$billingAttributesArray['parent_id'] = $customer->getId();
				}
				/* set shipping address parent_id and region_id */
				if(!empty($shippingAttributesArray)) {
					$regionShippingName = isset($Data['shipping_region']) ? $Data['shipping_region'] : '';
					if(isset($Data['shipping_country_id'])) { 
						$shipping_country_id = $Data['shipping_country_id']; 
					} else if(isset($Data['shipping_country'])) {  
						$shipping_country_id = $Data['shipping_country'];
					} else {
						throw new \Magento\Framework\Exception\LocalizedException(
							__('ERROR: CSV MUST HAS A COLUMN EITHER shipping_country or shipping_country_id')
						);
					}
					$shippingRegionId = $this->getRegionId($shipping_country_id, $regionShippingName);
					$shippingAttributesArray['region_id'] = $shippingRegionId;
					$shippingAttributesArray['parent_id'] = $customer->getId();
				}
				/*
				$addressOneData = [
					'firstname' => 'test firstname',
					'lastname' => 'test lastname',
					'street' => ['test street'],
					'city' => 'test city',
					'region_id' => '2',
					'country_id' => 'US',
					'postcode' => '01001',
					'telephone' => '+7000000001',
					'parent_id' => $customer->getId(),
				];
				*/
				
				//if($params['import_multiple_customer_address'] == "true") {  }
				
				//SET ADDRESS AS DEFAULT BILLING
				if(isset($Data["is_default_billing"]) && !empty($billingAttributesArray)) { 
					if($Data["is_default_billing"] == 1 && $Data["is_default_shipping"] == 1 ) {
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						$customer->setData('default_billing', $billingaddressInserted->getId());
						$customer->setData('default_shipping', $billingaddressInserted->getId());
					} else if($Data["is_default_billing"] == 1 && $Data["is_default_shipping"] == 0) { 
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						$customer->setData('default_billing', $billingaddressInserted->getId());
					} else if($Data["is_default_billing"] == 0 && $Data["is_default_shipping"] == 0) { 
						$billingaddressInserted = $this->_addressModel->setData($billingAttributesArray);
						$billingaddressInserted->save();
						#$customer->setData('default_billing', 0);
						#$customer->setData('default_shipping', 0);
					}
				}
				
				//SET ADDRESS AS DEFAULT SHIPPING
				if(isset($Data["is_default_shipping"]) && !empty($shippingAttributesArray)) { 
					if($Data["is_default_billing"] == 0 && $Data["is_default_shipping"] == 1) { 
						$shippingaddressInserted = $this->_addressModel->setData($shippingAttributesArray);
						$shippingaddressInserted->save(); 
						$customer->setData('default_shipping', $shippingaddressInserted->getId());
					} 
				} 
			} //end check on if we row contains a billing or shipping address
			
			/* Lets Import Customer Data */
			foreach($customer_attributes as $cal=>$val){
				if (!in_array($cal, $this->_disabledAttributes)) {
					if(isset($Data[$cal])) { $customer->setData($cal, $Data[$cal]); }
				}
			}
			if (isset($Data['is_subscribed'])) {
				#$customerModel->setData('is_subscribed', true);
				# get just generated subscriber
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$subscriber = $objectManager->create('Magento\Newsletter\Model\Subscriber')->loadByCustomerId($customer->getId());
				$subscriber->setStoreId($customer->getStoreId());
				$subscriber->setCustomerId($customer->getId());
				$subscriber->setSubscriberEmail($Data['email']);
				# change status to “subscribed” and save
				if($Data['is_subscribed'] == "1") {
					$subscriber->setSubscriberStatus(\Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED);
				} else {
					$subscriber->setSubscriberStatus(\Magento\Newsletter\Model\Subscriber::STATUS_UNSUBSCRIBED);
				}
				$subscriber->save();
			}
			// password change/set
			if (isset($Data['password'])) {
				if(strlen($Data['password'])) {
					$customer_password_hash = $customer->hashPassword($Data['password']);
					$customer->setData('password_hash', $customer_password_hash);
				}
			}
			
			try {
				$customer->save();
			} catch (\Exception $e) {
				throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
			}
			
			/*
			if($params['email_customer_password'] == "true") { 
				$customer->sendNewAccountEmail();
			} 
			*/
		
		}
    }
}